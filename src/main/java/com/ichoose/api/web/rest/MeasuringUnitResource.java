package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.MeasuringUnitService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.MeasuringUnitDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MeasuringUnit.
 */
@RestController
@RequestMapping("/api")
public class MeasuringUnitResource {

    private final Logger log = LoggerFactory.getLogger(MeasuringUnitResource.class);

    private static final String ENTITY_NAME = "measuringUnit";

    private final MeasuringUnitService measuringUnitService;

    public MeasuringUnitResource(MeasuringUnitService measuringUnitService) {
        this.measuringUnitService = measuringUnitService;
    }

    /**
     * POST  /measuring-units : Create a new measuringUnit.
     *
     * @param measuringUnitDTO the measuringUnitDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new measuringUnitDTO, or with status 400 (Bad Request) if the measuringUnit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/measuring-units")
    @Timed
    public ResponseEntity<MeasuringUnitDTO> createMeasuringUnit(@Valid @RequestBody MeasuringUnitDTO measuringUnitDTO) throws URISyntaxException {
        log.debug("REST request to save MeasuringUnit : {}", measuringUnitDTO);
        if (measuringUnitDTO.getId() != null) {
            throw new BadRequestAlertException("A new measuringUnit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeasuringUnitDTO result = measuringUnitService.save(measuringUnitDTO);
        return ResponseEntity.created(new URI("/api/measuring-units/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /measuring-units : Updates an existing measuringUnit.
     *
     * @param measuringUnitDTO the measuringUnitDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated measuringUnitDTO,
     * or with status 400 (Bad Request) if the measuringUnitDTO is not valid,
     * or with status 500 (Internal Server Error) if the measuringUnitDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/measuring-units")
    @Timed
    public ResponseEntity<MeasuringUnitDTO> updateMeasuringUnit(@Valid @RequestBody MeasuringUnitDTO measuringUnitDTO) throws URISyntaxException {
        log.debug("REST request to update MeasuringUnit : {}", measuringUnitDTO);
        if (measuringUnitDTO.getId() == null) {
            return createMeasuringUnit(measuringUnitDTO);
        }
        MeasuringUnitDTO result = measuringUnitService.save(measuringUnitDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, measuringUnitDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /measuring-units : get all the measuringUnits.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of measuringUnits in body
     */
    @GetMapping("/measuring-units")
    @Timed
    public ResponseEntity<List<MeasuringUnitDTO>> getAllMeasuringUnits(Pageable pageable) {
        log.debug("REST request to get a page of MeasuringUnits");
        Page<MeasuringUnitDTO> page = measuringUnitService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/measuring-units");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /measuring-units/:id : get the "id" measuringUnit.
     *
     * @param id the id of the measuringUnitDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the measuringUnitDTO, or with status 404 (Not Found)
     */
    @GetMapping("/measuring-units/{id}")
    @Timed
    public ResponseEntity<MeasuringUnitDTO> getMeasuringUnit(@PathVariable Long id) {
        log.debug("REST request to get MeasuringUnit : {}", id);
        MeasuringUnitDTO measuringUnitDTO = measuringUnitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(measuringUnitDTO));
    }

    /**
     * DELETE  /measuring-units/:id : delete the "id" measuringUnit.
     *
     * @param id the id of the measuringUnitDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/measuring-units/{id}")
    @Timed
    public ResponseEntity<Void> deleteMeasuringUnit(@PathVariable Long id) {
        log.debug("REST request to delete MeasuringUnit : {}", id);
        measuringUnitService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/measuring-units?query=:query : search for the measuringUnit corresponding
     * to the query.
     *
     * @param query the query of the measuringUnit search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/measuring-units")
    @Timed
    public ResponseEntity<List<MeasuringUnitDTO>> searchMeasuringUnits(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of MeasuringUnits for query {}", query);
        Page<MeasuringUnitDTO> page = measuringUnitService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/measuring-units");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
