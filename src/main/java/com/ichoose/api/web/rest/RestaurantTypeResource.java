package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.RestaurantTypeService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.RestaurantTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RestaurantType.
 */
@RestController
@RequestMapping("/api")
public class RestaurantTypeResource {

    private final Logger log = LoggerFactory.getLogger(RestaurantTypeResource.class);

    private static final String ENTITY_NAME = "restaurantType";

    private final RestaurantTypeService restaurantTypeService;

    public RestaurantTypeResource(RestaurantTypeService restaurantTypeService) {
        this.restaurantTypeService = restaurantTypeService;
    }

    /**
     * POST  /restaurant-types : Create a new restaurantType.
     *
     * @param restaurantTypeDTO the restaurantTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new restaurantTypeDTO, or with status 400 (Bad Request) if the restaurantType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/restaurant-types")
    @Timed
    public ResponseEntity<RestaurantTypeDTO> createRestaurantType(@Valid @RequestBody RestaurantTypeDTO restaurantTypeDTO) throws URISyntaxException {
        log.debug("REST request to save RestaurantType : {}", restaurantTypeDTO);
        if (restaurantTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new restaurantType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RestaurantTypeDTO result = restaurantTypeService.save(restaurantTypeDTO);
        return ResponseEntity.created(new URI("/api/restaurant-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /restaurant-types : Updates an existing restaurantType.
     *
     * @param restaurantTypeDTO the restaurantTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated restaurantTypeDTO,
     * or with status 400 (Bad Request) if the restaurantTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the restaurantTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/restaurant-types")
    @Timed
    public ResponseEntity<RestaurantTypeDTO> updateRestaurantType(@Valid @RequestBody RestaurantTypeDTO restaurantTypeDTO) throws URISyntaxException {
        log.debug("REST request to update RestaurantType : {}", restaurantTypeDTO);
        if (restaurantTypeDTO.getId() == null) {
            return createRestaurantType(restaurantTypeDTO);
        }
        RestaurantTypeDTO result = restaurantTypeService.save(restaurantTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, restaurantTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /restaurant-types : get all the restaurantTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of restaurantTypes in body
     */
    @GetMapping("/restaurant-types")
    @Timed
    public ResponseEntity<List<RestaurantTypeDTO>> getAllRestaurantTypes(Pageable pageable) {
        log.debug("REST request to get a page of RestaurantTypes");
        Page<RestaurantTypeDTO> page = restaurantTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/restaurant-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /restaurant-types/:id : get the "id" restaurantType.
     *
     * @param id the id of the restaurantTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the restaurantTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/restaurant-types/{id}")
    @Timed
    public ResponseEntity<RestaurantTypeDTO> getRestaurantType(@PathVariable Long id) {
        log.debug("REST request to get RestaurantType : {}", id);
        RestaurantTypeDTO restaurantTypeDTO = restaurantTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(restaurantTypeDTO));
    }

    /**
     * DELETE  /restaurant-types/:id : delete the "id" restaurantType.
     *
     * @param id the id of the restaurantTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/restaurant-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteRestaurantType(@PathVariable Long id) {
        log.debug("REST request to delete RestaurantType : {}", id);
        restaurantTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/restaurant-types?query=:query : search for the restaurantType corresponding
     * to the query.
     *
     * @param query the query of the restaurantType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/restaurant-types")
    @Timed
    public ResponseEntity<List<RestaurantTypeDTO>> searchRestaurantTypes(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of RestaurantTypes for query {}", query);
        Page<RestaurantTypeDTO> page = restaurantTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/restaurant-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
