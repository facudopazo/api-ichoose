/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ichoose.api.web.rest.vm;
