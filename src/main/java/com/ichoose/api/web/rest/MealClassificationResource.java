package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.MealClassificationService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.MealClassificationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MealClassification.
 */
@RestController
@RequestMapping("/api")
public class MealClassificationResource {

    private final Logger log = LoggerFactory.getLogger(MealClassificationResource.class);

    private static final String ENTITY_NAME = "mealClassification";

    private final MealClassificationService mealClassificationService;

    public MealClassificationResource(MealClassificationService mealClassificationService) {
        this.mealClassificationService = mealClassificationService;
    }

    /**
     * POST  /meal-classifications : Create a new mealClassification.
     *
     * @param mealClassificationDTO the mealClassificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mealClassificationDTO, or with status 400 (Bad Request) if the mealClassification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/meal-classifications")
    @Timed
    public ResponseEntity<MealClassificationDTO> createMealClassification(@Valid @RequestBody MealClassificationDTO mealClassificationDTO) throws URISyntaxException {
        log.debug("REST request to save MealClassification : {}", mealClassificationDTO);
        if (mealClassificationDTO.getId() != null) {
            throw new BadRequestAlertException("A new mealClassification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MealClassificationDTO result = mealClassificationService.save(mealClassificationDTO);
        return ResponseEntity.created(new URI("/api/meal-classifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /meal-classifications : Updates an existing mealClassification.
     *
     * @param mealClassificationDTO the mealClassificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mealClassificationDTO,
     * or with status 400 (Bad Request) if the mealClassificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the mealClassificationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/meal-classifications")
    @Timed
    public ResponseEntity<MealClassificationDTO> updateMealClassification(@Valid @RequestBody MealClassificationDTO mealClassificationDTO) throws URISyntaxException {
        log.debug("REST request to update MealClassification : {}", mealClassificationDTO);
        if (mealClassificationDTO.getId() == null) {
            return createMealClassification(mealClassificationDTO);
        }
        MealClassificationDTO result = mealClassificationService.save(mealClassificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, mealClassificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /meal-classifications : get all the mealClassifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of mealClassifications in body
     */
    @GetMapping("/meal-classifications")
    @Timed
    public ResponseEntity<List<MealClassificationDTO>> getAllMealClassifications(Pageable pageable) {
        log.debug("REST request to get a page of MealClassifications");
        Page<MealClassificationDTO> page = mealClassificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/meal-classifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /meal-classifications/:id : get the "id" mealClassification.
     *
     * @param id the id of the mealClassificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mealClassificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/meal-classifications/{id}")
    @Timed
    public ResponseEntity<MealClassificationDTO> getMealClassification(@PathVariable Long id) {
        log.debug("REST request to get MealClassification : {}", id);
        MealClassificationDTO mealClassificationDTO = mealClassificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(mealClassificationDTO));
    }

    /**
     * DELETE  /meal-classifications/:id : delete the "id" mealClassification.
     *
     * @param id the id of the mealClassificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/meal-classifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteMealClassification(@PathVariable Long id) {
        log.debug("REST request to delete MealClassification : {}", id);
        mealClassificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/meal-classifications?query=:query : search for the mealClassification corresponding
     * to the query.
     *
     * @param query the query of the mealClassification search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/meal-classifications")
    @Timed
    public ResponseEntity<List<MealClassificationDTO>> searchMealClassifications(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of MealClassifications for query {}", query);
        Page<MealClassificationDTO> page = mealClassificationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/meal-classifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
