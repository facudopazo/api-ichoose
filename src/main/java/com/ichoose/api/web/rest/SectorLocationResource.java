package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.SectorLocationService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.SectorLocationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SectorLocation.
 */
@RestController
@RequestMapping("/api")
public class SectorLocationResource {

    private final Logger log = LoggerFactory.getLogger(SectorLocationResource.class);

    private static final String ENTITY_NAME = "sectorLocation";

    private final SectorLocationService sectorLocationService;

    public SectorLocationResource(SectorLocationService sectorLocationService) {
        this.sectorLocationService = sectorLocationService;
    }

    /**
     * POST  /sector-locations : Create a new sectorLocation.
     *
     * @param sectorLocationDTO the sectorLocationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sectorLocationDTO, or with status 400 (Bad Request) if the sectorLocation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sector-locations")
    @Timed
    public ResponseEntity<SectorLocationDTO> createSectorLocation(@Valid @RequestBody SectorLocationDTO sectorLocationDTO) throws URISyntaxException {
        log.debug("REST request to save SectorLocation : {}", sectorLocationDTO);
        if (sectorLocationDTO.getId() != null) {
            throw new BadRequestAlertException("A new sectorLocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SectorLocationDTO result = sectorLocationService.save(sectorLocationDTO);
        return ResponseEntity.created(new URI("/api/sector-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sector-locations : Updates an existing sectorLocation.
     *
     * @param sectorLocationDTO the sectorLocationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sectorLocationDTO,
     * or with status 400 (Bad Request) if the sectorLocationDTO is not valid,
     * or with status 500 (Internal Server Error) if the sectorLocationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sector-locations")
    @Timed
    public ResponseEntity<SectorLocationDTO> updateSectorLocation(@Valid @RequestBody SectorLocationDTO sectorLocationDTO) throws URISyntaxException {
        log.debug("REST request to update SectorLocation : {}", sectorLocationDTO);
        if (sectorLocationDTO.getId() == null) {
            return createSectorLocation(sectorLocationDTO);
        }
        SectorLocationDTO result = sectorLocationService.save(sectorLocationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sectorLocationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sector-locations : get all the sectorLocations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sectorLocations in body
     */
    @GetMapping("/sector-locations")
    @Timed
    public ResponseEntity<List<SectorLocationDTO>> getAllSectorLocations(Pageable pageable) {
        log.debug("REST request to get a page of SectorLocations");
        Page<SectorLocationDTO> page = sectorLocationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sector-locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sector-locations/:id : get the "id" sectorLocation.
     *
     * @param id the id of the sectorLocationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sectorLocationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sector-locations/{id}")
    @Timed
    public ResponseEntity<SectorLocationDTO> getSectorLocation(@PathVariable Long id) {
        log.debug("REST request to get SectorLocation : {}", id);
        SectorLocationDTO sectorLocationDTO = sectorLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sectorLocationDTO));
    }

    /**
     * DELETE  /sector-locations/:id : delete the "id" sectorLocation.
     *
     * @param id the id of the sectorLocationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sector-locations/{id}")
    @Timed
    public ResponseEntity<Void> deleteSectorLocation(@PathVariable Long id) {
        log.debug("REST request to delete SectorLocation : {}", id);
        sectorLocationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/sector-locations?query=:query : search for the sectorLocation corresponding
     * to the query.
     *
     * @param query the query of the sectorLocation search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sector-locations")
    @Timed
    public ResponseEntity<List<SectorLocationDTO>> searchSectorLocations(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of SectorLocations for query {}", query);
        Page<SectorLocationDTO> page = sectorLocationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sector-locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
