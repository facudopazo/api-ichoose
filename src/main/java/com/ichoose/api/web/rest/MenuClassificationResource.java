package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.MenuClassificationService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.MenuClassificationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MenuClassification.
 */
@RestController
@RequestMapping("/api")
public class MenuClassificationResource {

    private final Logger log = LoggerFactory.getLogger(MenuClassificationResource.class);

    private static final String ENTITY_NAME = "menuClassification";

    private final MenuClassificationService menuClassificationService;

    public MenuClassificationResource(MenuClassificationService menuClassificationService) {
        this.menuClassificationService = menuClassificationService;
    }

    /**
     * POST  /menu-classifications : Create a new menuClassification.
     *
     * @param menuClassificationDTO the menuClassificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new menuClassificationDTO, or with status 400 (Bad Request) if the menuClassification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/menu-classifications")
    @Timed
    public ResponseEntity<MenuClassificationDTO> createMenuClassification(@Valid @RequestBody MenuClassificationDTO menuClassificationDTO) throws URISyntaxException {
        log.debug("REST request to save MenuClassification : {}", menuClassificationDTO);
        if (menuClassificationDTO.getId() != null) {
            throw new BadRequestAlertException("A new menuClassification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MenuClassificationDTO result = menuClassificationService.save(menuClassificationDTO);
        return ResponseEntity.created(new URI("/api/menu-classifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /menu-classifications : Updates an existing menuClassification.
     *
     * @param menuClassificationDTO the menuClassificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated menuClassificationDTO,
     * or with status 400 (Bad Request) if the menuClassificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the menuClassificationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/menu-classifications")
    @Timed
    public ResponseEntity<MenuClassificationDTO> updateMenuClassification(@Valid @RequestBody MenuClassificationDTO menuClassificationDTO) throws URISyntaxException {
        log.debug("REST request to update MenuClassification : {}", menuClassificationDTO);
        if (menuClassificationDTO.getId() == null) {
            return createMenuClassification(menuClassificationDTO);
        }
        MenuClassificationDTO result = menuClassificationService.save(menuClassificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, menuClassificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /menu-classifications : get all the menuClassifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of menuClassifications in body
     */
    @GetMapping("/menu-classifications")
    @Timed
    public ResponseEntity<List<MenuClassificationDTO>> getAllMenuClassifications(Pageable pageable) {
        log.debug("REST request to get a page of MenuClassifications");
        Page<MenuClassificationDTO> page = menuClassificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-classifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /menu-classifications/:id : get the "id" menuClassification.
     *
     * @param id the id of the menuClassificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuClassificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/menu-classifications/{id}")
    @Timed
    public ResponseEntity<MenuClassificationDTO> getMenuClassification(@PathVariable Long id) {
        log.debug("REST request to get MenuClassification : {}", id);
        MenuClassificationDTO menuClassificationDTO = menuClassificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(menuClassificationDTO));
    }

    /**
     * DELETE  /menu-classifications/:id : delete the "id" menuClassification.
     *
     * @param id the id of the menuClassificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/menu-classifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteMenuClassification(@PathVariable Long id) {
        log.debug("REST request to delete MenuClassification : {}", id);
        menuClassificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/menu-classifications?query=:query : search for the menuClassification corresponding
     * to the query.
     *
     * @param query the query of the menuClassification search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/menu-classifications")
    @Timed
    public ResponseEntity<List<MenuClassificationDTO>> searchMenuClassifications(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of MenuClassifications for query {}", query);
        Page<MenuClassificationDTO> page = menuClassificationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/menu-classifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
