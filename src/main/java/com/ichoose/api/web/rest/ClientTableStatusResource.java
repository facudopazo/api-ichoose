package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.ClientTableStatusService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.ClientTableStatusDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClientTableStatus.
 */
@RestController
@RequestMapping("/api")
public class ClientTableStatusResource {

    private final Logger log = LoggerFactory.getLogger(ClientTableStatusResource.class);

    private static final String ENTITY_NAME = "clientTableStatus";

    private final ClientTableStatusService clientTableStatusService;

    public ClientTableStatusResource(ClientTableStatusService clientTableStatusService) {
        this.clientTableStatusService = clientTableStatusService;
    }

    /**
     * POST  /client-table-statuses : Create a new clientTableStatus.
     *
     * @param clientTableStatusDTO the clientTableStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientTableStatusDTO, or with status 400 (Bad Request) if the clientTableStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-table-statuses")
    @Timed
    public ResponseEntity<ClientTableStatusDTO> createClientTableStatus(@Valid @RequestBody ClientTableStatusDTO clientTableStatusDTO) throws URISyntaxException {
        log.debug("REST request to save ClientTableStatus : {}", clientTableStatusDTO);
        if (clientTableStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientTableStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientTableStatusDTO result = clientTableStatusService.save(clientTableStatusDTO);
        return ResponseEntity.created(new URI("/api/client-table-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-table-statuses : Updates an existing clientTableStatus.
     *
     * @param clientTableStatusDTO the clientTableStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientTableStatusDTO,
     * or with status 400 (Bad Request) if the clientTableStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientTableStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-table-statuses")
    @Timed
    public ResponseEntity<ClientTableStatusDTO> updateClientTableStatus(@Valid @RequestBody ClientTableStatusDTO clientTableStatusDTO) throws URISyntaxException {
        log.debug("REST request to update ClientTableStatus : {}", clientTableStatusDTO);
        if (clientTableStatusDTO.getId() == null) {
            return createClientTableStatus(clientTableStatusDTO);
        }
        ClientTableStatusDTO result = clientTableStatusService.save(clientTableStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientTableStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-table-statuses : get all the clientTableStatuses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientTableStatuses in body
     */
    @GetMapping("/client-table-statuses")
    @Timed
    public ResponseEntity<List<ClientTableStatusDTO>> getAllClientTableStatuses(Pageable pageable) {
        log.debug("REST request to get a page of ClientTableStatuses");
        Page<ClientTableStatusDTO> page = clientTableStatusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-table-statuses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-table-statuses/:id : get the "id" clientTableStatus.
     *
     * @param id the id of the clientTableStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientTableStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-table-statuses/{id}")
    @Timed
    public ResponseEntity<ClientTableStatusDTO> getClientTableStatus(@PathVariable Long id) {
        log.debug("REST request to get ClientTableStatus : {}", id);
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientTableStatusDTO));
    }

    /**
     * DELETE  /client-table-statuses/:id : delete the "id" clientTableStatus.
     *
     * @param id the id of the clientTableStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-table-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientTableStatus(@PathVariable Long id) {
        log.debug("REST request to delete ClientTableStatus : {}", id);
        clientTableStatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/client-table-statuses?query=:query : search for the clientTableStatus corresponding
     * to the query.
     *
     * @param query the query of the clientTableStatus search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/client-table-statuses")
    @Timed
    public ResponseEntity<List<ClientTableStatusDTO>> searchClientTableStatuses(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ClientTableStatuses for query {}", query);
        Page<ClientTableStatusDTO> page = clientTableStatusService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/client-table-statuses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
