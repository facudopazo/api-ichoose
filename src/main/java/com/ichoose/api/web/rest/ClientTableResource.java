package com.ichoose.api.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ichoose.api.service.ClientTableService;
import com.ichoose.api.web.rest.errors.BadRequestAlertException;
import com.ichoose.api.web.rest.util.HeaderUtil;
import com.ichoose.api.web.rest.util.PaginationUtil;
import com.ichoose.api.service.dto.ClientTableDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClientTable.
 */
@RestController
@RequestMapping("/api")
public class ClientTableResource {

    private final Logger log = LoggerFactory.getLogger(ClientTableResource.class);

    private static final String ENTITY_NAME = "clientTable";

    private final ClientTableService clientTableService;

    public ClientTableResource(ClientTableService clientTableService) {
        this.clientTableService = clientTableService;
    }

    /**
     * POST  /client-tables : Create a new clientTable.
     *
     * @param clientTableDTO the clientTableDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientTableDTO, or with status 400 (Bad Request) if the clientTable has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-tables")
    @Timed
    public ResponseEntity<ClientTableDTO> createClientTable(@Valid @RequestBody ClientTableDTO clientTableDTO) throws URISyntaxException {
        log.debug("REST request to save ClientTable : {}", clientTableDTO);
        if (clientTableDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientTable cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientTableDTO result = clientTableService.save(clientTableDTO);
        return ResponseEntity.created(new URI("/api/client-tables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-tables : Updates an existing clientTable.
     *
     * @param clientTableDTO the clientTableDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientTableDTO,
     * or with status 400 (Bad Request) if the clientTableDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientTableDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-tables")
    @Timed
    public ResponseEntity<ClientTableDTO> updateClientTable(@Valid @RequestBody ClientTableDTO clientTableDTO) throws URISyntaxException {
        log.debug("REST request to update ClientTable : {}", clientTableDTO);
        if (clientTableDTO.getId() == null) {
            return createClientTable(clientTableDTO);
        }
        ClientTableDTO result = clientTableService.save(clientTableDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientTableDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-tables : get all the clientTables.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientTables in body
     */
    @GetMapping("/client-tables")
    @Timed
    public ResponseEntity<List<ClientTableDTO>> getAllClientTables(Pageable pageable) {
        log.debug("REST request to get a page of ClientTables");
        Page<ClientTableDTO> page = clientTableService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-tables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-tables/:id : get the "id" clientTable.
     *
     * @param id the id of the clientTableDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientTableDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-tables/{id}")
    @Timed
    public ResponseEntity<ClientTableDTO> getClientTable(@PathVariable Long id) {
        log.debug("REST request to get ClientTable : {}", id);
        ClientTableDTO clientTableDTO = clientTableService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientTableDTO));
    }

    /**
     * DELETE  /client-tables/:id : delete the "id" clientTable.
     *
     * @param id the id of the clientTableDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-tables/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientTable(@PathVariable Long id) {
        log.debug("REST request to delete ClientTable : {}", id);
        clientTableService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/client-tables?query=:query : search for the clientTable corresponding
     * to the query.
     *
     * @param query the query of the clientTable search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/client-tables")
    @Timed
    public ResponseEntity<List<ClientTableDTO>> searchClientTables(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ClientTables for query {}", query);
        Page<ClientTableDTO> page = clientTableService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/client-tables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
