package com.ichoose.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Document(indexName = "restaurant")
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "cuit", nullable = false)
    private String cuit;

    @Column(name = "cbu")
    private String cbu;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Location location;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Picture> pictures = new HashSet<>();

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "restaurant")
    @JsonIgnore
    private Set<Sector> sectors = new HashSet<>();

    @ManyToOne
    private RestaurantType restaurantType;

    @ManyToMany
    @JoinTable(name = "restaurant_meal_classification",
               joinColumns = @JoinColumn(name="restaurants_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="meal_classifications_id", referencedColumnName="id"))
    private Set<MealClassification> mealClassifications = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Restaurant name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Restaurant description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCuit() {
        return cuit;
    }

    public Restaurant cuit(String cuit) {
        this.cuit = cuit;
        return this;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getCbu() {
        return cbu;
    }

    public Restaurant cbu(String cbu) {
        this.cbu = cbu;
        return this;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public Location getLocation() {
        return location;
    }

    public Restaurant location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<Picture> getPictures() {
        return pictures;
    }

    public Restaurant pictures(Set<Picture> pictures) {
        this.pictures = pictures;
        return this;
    }

    public Restaurant addPictures(Picture picture) {
        this.pictures.add(picture);
        picture.setRestaurant(this);
        return this;
    }

    public Restaurant removePictures(Picture picture) {
        this.pictures.remove(picture);
        picture.setRestaurant(null);
        return this;
    }

    public void setPictures(Set<Picture> pictures) {
        this.pictures = pictures;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Restaurant roles(Set<Role> roles) {
        this.roles = roles;
        return this;
    }

    public Restaurant addRoles(Role role) {
        this.roles.add(role);
        role.setRestaurant(this);
        return this;
    }

    public Restaurant removeRoles(Role role) {
        this.roles.remove(role);
        role.setRestaurant(null);
        return this;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Sector> getSectors() {
        return sectors;
    }

    public Restaurant sectors(Set<Sector> sectors) {
        this.sectors = sectors;
        return this;
    }

    public Restaurant addSectors(Sector sector) {
        this.sectors.add(sector);
        sector.setRestaurant(this);
        return this;
    }

    public Restaurant removeSectors(Sector sector) {
        this.sectors.remove(sector);
        sector.setRestaurant(null);
        return this;
    }

    public void setSectors(Set<Sector> sectors) {
        this.sectors = sectors;
    }

    public RestaurantType getRestaurantType() {
        return restaurantType;
    }

    public Restaurant restaurantType(RestaurantType restaurantType) {
        this.restaurantType = restaurantType;
        return this;
    }

    public void setRestaurantType(RestaurantType restaurantType) {
        this.restaurantType = restaurantType;
    }

    public Set<MealClassification> getMealClassifications() {
        return mealClassifications;
    }

    public Restaurant mealClassifications(Set<MealClassification> mealClassifications) {
        this.mealClassifications = mealClassifications;
        return this;
    }

    public Restaurant addMealClassification(MealClassification mealClassification) {
        this.mealClassifications.add(mealClassification);
        return this;
    }

    public Restaurant removeMealClassification(MealClassification mealClassification) {
        this.mealClassifications.remove(mealClassification);
        return this;
    }

    public void setMealClassifications(Set<MealClassification> mealClassifications) {
        this.mealClassifications = mealClassifications;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Restaurant restaurant = (Restaurant) o;
        if (restaurant.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), restaurant.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", cuit='" + getCuit() + "'" +
            ", cbu='" + getCbu() + "'" +
            "}";
    }
}
