package com.ichoose.api.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.ichoose.api.domain.enumeration.DestinationType;

/**
 * A Menu.
 */
@Entity
@Table(name = "menu")
@Document(indexName = "menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Double quantity;

    @Enumerated(EnumType.STRING)
    @Column(name = "destination")
    private DestinationType destination;

    @Column(name = "disabled")
    private Boolean disabled;

    @Column(name = "delete_date")
    private ZonedDateTime deleteDate;

    @ManyToOne
    private MeasuringUnit measuringUnit;

    @ManyToOne
    private MenuClassification classification;

    @ManyToOne
    private Restaurant restaurant;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "menu_pictures",
               joinColumns = @JoinColumn(name="menus_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="pictures_id", referencedColumnName="id"))
    private Set<Picture> pictures = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "menu_subclassifications",
               joinColumns = @JoinColumn(name="menus_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="subclassifications_id", referencedColumnName="id"))
    private Set<MenuSubclassification> subclassifications = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Menu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Menu description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public Menu price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Menu quantity(Double quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public DestinationType getDestination() {
        return destination;
    }

    public Menu destination(DestinationType destination) {
        this.destination = destination;
        return this;
    }

    public void setDestination(DestinationType destination) {
        this.destination = destination;
    }

    public Boolean isDisabled() {
        return disabled;
    }

    public Menu disabled(Boolean disabled) {
        this.disabled = disabled;
        return this;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public ZonedDateTime getDeleteDate() {
        return deleteDate;
    }

    public Menu deleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }

    public void setDeleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public MeasuringUnit getMeasuringUnit() {
        return measuringUnit;
    }

    public Menu measuringUnit(MeasuringUnit measuringUnit) {
        this.measuringUnit = measuringUnit;
        return this;
    }

    public void setMeasuringUnit(MeasuringUnit measuringUnit) {
        this.measuringUnit = measuringUnit;
    }

    public MenuClassification getClassification() {
        return classification;
    }

    public Menu classification(MenuClassification menuClassification) {
        this.classification = menuClassification;
        return this;
    }

    public void setClassification(MenuClassification menuClassification) {
        this.classification = menuClassification;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Menu restaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Set<Picture> getPictures() {
        return pictures;
    }

    public Menu pictures(Set<Picture> pictures) {
        this.pictures = pictures;
        return this;
    }

    public Menu addPictures(Picture picture) {
        this.pictures.add(picture);
        return this;
    }

    public Menu removePictures(Picture picture) {
        this.pictures.remove(picture);
        return this;
    }

    public void setPictures(Set<Picture> pictures) {
        this.pictures = pictures;
    }

    public Set<MenuSubclassification> getSubclassifications() {
        return subclassifications;
    }

    public Menu subclassifications(Set<MenuSubclassification> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
        return this;
    }

    public Menu addSubclassifications(MenuSubclassification menuSubclassification) {
        this.subclassifications.add(menuSubclassification);
        return this;
    }

    public Menu removeSubclassifications(MenuSubclassification menuSubclassification) {
        this.subclassifications.remove(menuSubclassification);
        return this;
    }

    public void setSubclassifications(Set<MenuSubclassification> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Menu menu = (Menu) o;
        if (menu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Menu{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", price=" + getPrice() +
            ", quantity=" + getQuantity() +
            ", destination='" + getDestination() + "'" +
            ", disabled='" + isDisabled() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
