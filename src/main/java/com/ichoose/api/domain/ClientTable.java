package com.ichoose.api.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ClientTable.
 */
@Entity
@Table(name = "client_table")
@Document(indexName = "clienttable")
public class ClientTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_number", nullable = false)
    private String number;

    @NotNull
    @Column(name = "chairs", nullable = false)
    private Integer chairs;

    @NotNull
    @Column(name = "modifiable", nullable = false)
    private Boolean modifiable;

    @Column(name = "delete_date")
    private ZonedDateTime deleteDate;

    @ManyToOne
    private Sector sector;

    @ManyToOne
    private ClientTableStatus status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public ClientTable number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getChairs() {
        return chairs;
    }

    public ClientTable chairs(Integer chairs) {
        this.chairs = chairs;
        return this;
    }

    public void setChairs(Integer chairs) {
        this.chairs = chairs;
    }

    public Boolean isModifiable() {
        return modifiable;
    }

    public ClientTable modifiable(Boolean modifiable) {
        this.modifiable = modifiable;
        return this;
    }

    public void setModifiable(Boolean modifiable) {
        this.modifiable = modifiable;
    }

    public ZonedDateTime getDeleteDate() {
        return deleteDate;
    }

    public ClientTable deleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }

    public void setDeleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Sector getSector() {
        return sector;
    }

    public ClientTable sector(Sector sector) {
        this.sector = sector;
        return this;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public ClientTableStatus getStatus() {
        return status;
    }

    public ClientTable status(ClientTableStatus clientTableStatus) {
        this.status = clientTableStatus;
        return this;
    }

    public void setStatus(ClientTableStatus clientTableStatus) {
        this.status = clientTableStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientTable clientTable = (ClientTable) o;
        if (clientTable.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientTable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientTable{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", chairs=" + getChairs() +
            ", modifiable='" + isModifiable() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
