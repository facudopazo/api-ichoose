package com.ichoose.api.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MenuClassification.
 */
@Entity
@Table(name = "menu_classification")
@Document(indexName = "menuclassification")
public class MenuClassification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(name = "menu_classification_subclassifications",
               joinColumns = @JoinColumn(name="menu_classifications_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="subclassifications_id", referencedColumnName="id"))
    private Set<MenuSubclassification> subclassifications = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MenuClassification name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MenuSubclassification> getSubclassifications() {
        return subclassifications;
    }

    public MenuClassification subclassifications(Set<MenuSubclassification> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
        return this;
    }

    public MenuClassification addSubclassifications(MenuSubclassification menuSubclassification) {
        this.subclassifications.add(menuSubclassification);
        return this;
    }

    public MenuClassification removeSubclassifications(MenuSubclassification menuSubclassification) {
        this.subclassifications.remove(menuSubclassification);
        return this;
    }

    public void setSubclassifications(Set<MenuSubclassification> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuClassification menuClassification = (MenuClassification) o;
        if (menuClassification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuClassification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuClassification{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
