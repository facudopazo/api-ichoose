package com.ichoose.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Sector.
 */
@Entity
@Table(name = "sector")
@Document(indexName = "sector")
public class Sector implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "accesibility", nullable = false)
    private Boolean accesibility;

    @Column(name = "delete_date")
    private ZonedDateTime deleteDate;

    @ManyToOne
    private Restaurant restaurant;

    @OneToMany(mappedBy = "sector")
    @JsonIgnore
    private Set<ClientTable> tables = new HashSet<>();

    @ManyToOne
    private SectorLocation location;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Sector name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isAccesibility() {
        return accesibility;
    }

    public Sector accesibility(Boolean accesibility) {
        this.accesibility = accesibility;
        return this;
    }

    public void setAccesibility(Boolean accesibility) {
        this.accesibility = accesibility;
    }

    public ZonedDateTime getDeleteDate() {
        return deleteDate;
    }

    public Sector deleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }

    public void setDeleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Sector restaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Set<ClientTable> getTables() {
        return tables;
    }

    public Sector tables(Set<ClientTable> clientTables) {
        this.tables = clientTables;
        return this;
    }

    public Sector addTables(ClientTable clientTable) {
        this.tables.add(clientTable);
        clientTable.setSector(this);
        return this;
    }

    public Sector removeTables(ClientTable clientTable) {
        this.tables.remove(clientTable);
        clientTable.setSector(null);
        return this;
    }

    public void setTables(Set<ClientTable> clientTables) {
        this.tables = clientTables;
    }

    public SectorLocation getLocation() {
        return location;
    }

    public Sector location(SectorLocation sectorLocation) {
        this.location = sectorLocation;
        return this;
    }

    public void setLocation(SectorLocation sectorLocation) {
        this.location = sectorLocation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sector sector = (Sector) o;
        if (sector.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sector.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sector{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", accesibility='" + isAccesibility() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
