package com.ichoose.api.domain.enumeration;

/**
 * The DestinationType enumeration.
 */
public enum DestinationType {
    BAR, COOKER
}
