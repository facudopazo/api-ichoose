package com.ichoose.api.domain.enumeration;

/**
 * The RoleType enumeration.
 */
public enum RoleType {
    ADMIN, WAITER
}
