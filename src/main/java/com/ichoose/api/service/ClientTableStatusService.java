package com.ichoose.api.service;

import com.ichoose.api.domain.ClientTableStatus;
import com.ichoose.api.repository.ClientTableStatusRepository;
import com.ichoose.api.repository.search.ClientTableStatusSearchRepository;
import com.ichoose.api.service.dto.ClientTableStatusDTO;
import com.ichoose.api.service.mapper.ClientTableStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ClientTableStatus.
 */
@Service
@Transactional
public class ClientTableStatusService {

    private final Logger log = LoggerFactory.getLogger(ClientTableStatusService.class);

    private final ClientTableStatusRepository clientTableStatusRepository;

    private final ClientTableStatusMapper clientTableStatusMapper;

    private final ClientTableStatusSearchRepository clientTableStatusSearchRepository;

    public ClientTableStatusService(ClientTableStatusRepository clientTableStatusRepository, ClientTableStatusMapper clientTableStatusMapper, ClientTableStatusSearchRepository clientTableStatusSearchRepository) {
        this.clientTableStatusRepository = clientTableStatusRepository;
        this.clientTableStatusMapper = clientTableStatusMapper;
        this.clientTableStatusSearchRepository = clientTableStatusSearchRepository;
    }

    /**
     * Save a clientTableStatus.
     *
     * @param clientTableStatusDTO the entity to save
     * @return the persisted entity
     */
    public ClientTableStatusDTO save(ClientTableStatusDTO clientTableStatusDTO) {
        log.debug("Request to save ClientTableStatus : {}", clientTableStatusDTO);
        ClientTableStatus clientTableStatus = clientTableStatusMapper.toEntity(clientTableStatusDTO);
        clientTableStatus = clientTableStatusRepository.save(clientTableStatus);
        ClientTableStatusDTO result = clientTableStatusMapper.toDto(clientTableStatus);
        clientTableStatusSearchRepository.save(clientTableStatus);
        return result;
    }

    /**
     * Get all the clientTableStatuses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientTableStatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientTableStatuses");
        return clientTableStatusRepository.findAll(pageable)
            .map(clientTableStatusMapper::toDto);
    }

    /**
     * Get one clientTableStatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientTableStatusDTO findOne(Long id) {
        log.debug("Request to get ClientTableStatus : {}", id);
        ClientTableStatus clientTableStatus = clientTableStatusRepository.findOne(id);
        return clientTableStatusMapper.toDto(clientTableStatus);
    }

    /**
     * Delete the clientTableStatus by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientTableStatus : {}", id);
        clientTableStatusRepository.delete(id);
        clientTableStatusSearchRepository.delete(id);
    }

    /**
     * Search for the clientTableStatus corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientTableStatusDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ClientTableStatuses for query {}", query);
        Page<ClientTableStatus> result = clientTableStatusSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(clientTableStatusMapper::toDto);
    }
}
