package com.ichoose.api.service;

import com.ichoose.api.domain.SectorLocation;
import com.ichoose.api.repository.SectorLocationRepository;
import com.ichoose.api.repository.search.SectorLocationSearchRepository;
import com.ichoose.api.service.dto.SectorLocationDTO;
import com.ichoose.api.service.mapper.SectorLocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SectorLocation.
 */
@Service
@Transactional
public class SectorLocationService {

    private final Logger log = LoggerFactory.getLogger(SectorLocationService.class);

    private final SectorLocationRepository sectorLocationRepository;

    private final SectorLocationMapper sectorLocationMapper;

    private final SectorLocationSearchRepository sectorLocationSearchRepository;

    public SectorLocationService(SectorLocationRepository sectorLocationRepository, SectorLocationMapper sectorLocationMapper, SectorLocationSearchRepository sectorLocationSearchRepository) {
        this.sectorLocationRepository = sectorLocationRepository;
        this.sectorLocationMapper = sectorLocationMapper;
        this.sectorLocationSearchRepository = sectorLocationSearchRepository;
    }

    /**
     * Save a sectorLocation.
     *
     * @param sectorLocationDTO the entity to save
     * @return the persisted entity
     */
    public SectorLocationDTO save(SectorLocationDTO sectorLocationDTO) {
        log.debug("Request to save SectorLocation : {}", sectorLocationDTO);
        SectorLocation sectorLocation = sectorLocationMapper.toEntity(sectorLocationDTO);
        sectorLocation = sectorLocationRepository.save(sectorLocation);
        SectorLocationDTO result = sectorLocationMapper.toDto(sectorLocation);
        sectorLocationSearchRepository.save(sectorLocation);
        return result;
    }

    /**
     * Get all the sectorLocations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SectorLocationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SectorLocations");
        return sectorLocationRepository.findAll(pageable)
            .map(sectorLocationMapper::toDto);
    }

    /**
     * Get one sectorLocation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SectorLocationDTO findOne(Long id) {
        log.debug("Request to get SectorLocation : {}", id);
        SectorLocation sectorLocation = sectorLocationRepository.findOne(id);
        return sectorLocationMapper.toDto(sectorLocation);
    }

    /**
     * Delete the sectorLocation by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SectorLocation : {}", id);
        sectorLocationRepository.delete(id);
        sectorLocationSearchRepository.delete(id);
    }

    /**
     * Search for the sectorLocation corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SectorLocationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SectorLocations for query {}", query);
        Page<SectorLocation> result = sectorLocationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(sectorLocationMapper::toDto);
    }
}
