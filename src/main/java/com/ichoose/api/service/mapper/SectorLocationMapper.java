package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.SectorLocationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SectorLocation and its DTO SectorLocationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SectorLocationMapper extends EntityMapper<SectorLocationDTO, SectorLocation> {



    default SectorLocation fromId(Long id) {
        if (id == null) {
            return null;
        }
        SectorLocation sectorLocation = new SectorLocation();
        sectorLocation.setId(id);
        return sectorLocation;
    }
}
