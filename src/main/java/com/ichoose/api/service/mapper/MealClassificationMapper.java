package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.MealClassificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MealClassification and its DTO MealClassificationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MealClassificationMapper extends EntityMapper<MealClassificationDTO, MealClassification> {



    default MealClassification fromId(Long id) {
        if (id == null) {
            return null;
        }
        MealClassification mealClassification = new MealClassification();
        mealClassification.setId(id);
        return mealClassification;
    }
}
