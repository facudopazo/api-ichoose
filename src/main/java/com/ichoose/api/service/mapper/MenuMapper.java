package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.MenuDTO;

import java.util.List;

import org.mapstruct.*;

/**
 * Mapper for the entity Menu and its DTO MenuDTO.
 */
@Mapper(componentModel = "spring", uses = {MeasuringUnitMapper.class, MenuClassificationMapper.class, RestaurantMapper.class, PictureMapper.class, MenuSubclassificationMapper.class})
public interface MenuMapper extends EntityMapper<MenuDTO, Menu> {

    @Mapping(source = "measuringUnit.id", target = "measuringUnitId")
    @Mapping(source = "classification.id", target = "classificationId")
    @Mapping(source = "measuringUnit", target = "measuringUnit")
    @Mapping(source = "classification", target = "classification")
    @Mapping(source = "restaurant.id", target = "restaurantId")
    MenuDTO toDto(Menu menu);

    @Mapping(source = "measuringUnitId", target = "measuringUnit")
    @Mapping(source = "classificationId", target = "classification")
    @Mapping(source = "restaurantId", target = "restaurant")
    Menu toEntity(MenuDTO menuDTO);
    
    default Menu fromId(Long id) {
        if (id == null) {
            return null;
        }
        Menu menu = new Menu();
        menu.setId(id);
        return menu;
    }
}
