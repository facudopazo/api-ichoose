package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.RestaurantDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Restaurant and its DTO RestaurantDTO.
 */
@Mapper(componentModel = "spring", uses = {LocationMapper.class, RestaurantTypeMapper.class, MealClassificationMapper.class, PictureMapper.class})
public interface RestaurantMapper extends EntityMapper<RestaurantDTO, Restaurant> {

    @Mapping(source = "location", target = "location")
    @Mapping(source = "restaurantType.id", target = "restaurantTypeId")
    RestaurantDTO toDto(Restaurant restaurant);

    @Mapping(source = "location", target = "location")
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "sectors", ignore = true)
    @Mapping(source = "restaurantTypeId", target = "restaurantType")
    Restaurant toEntity(RestaurantDTO restaurantDTO);

    default Restaurant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Restaurant restaurant = new Restaurant();
        restaurant.setId(id);
        return restaurant;
    }
}
