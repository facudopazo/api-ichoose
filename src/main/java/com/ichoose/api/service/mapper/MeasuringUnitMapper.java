package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.MeasuringUnitDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MeasuringUnit and its DTO MeasuringUnitDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MeasuringUnitMapper extends EntityMapper<MeasuringUnitDTO, MeasuringUnit> {



    default MeasuringUnit fromId(Long id) {
        if (id == null) {
            return null;
        }
        MeasuringUnit measuringUnit = new MeasuringUnit();
        measuringUnit.setId(id);
        return measuringUnit;
    }
}
