package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.RestaurantTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RestaurantType and its DTO RestaurantTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RestaurantTypeMapper extends EntityMapper<RestaurantTypeDTO, RestaurantType> {



    default RestaurantType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RestaurantType restaurantType = new RestaurantType();
        restaurantType.setId(id);
        return restaurantType;
    }
}
