package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.MenuClassificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MenuClassification and its DTO MenuClassificationDTO.
 */
@Mapper(componentModel = "spring", uses = {MenuSubclassificationMapper.class})
public interface MenuClassificationMapper extends EntityMapper<MenuClassificationDTO, MenuClassification> {



    default MenuClassification fromId(Long id) {
        if (id == null) {
            return null;
        }
        MenuClassification menuClassification = new MenuClassification();
        menuClassification.setId(id);
        return menuClassification;
    }
}
