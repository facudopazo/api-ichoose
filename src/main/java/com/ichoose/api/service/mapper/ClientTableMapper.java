package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.ClientTableDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientTable and its DTO ClientTableDTO.
 */
@Mapper(componentModel = "spring", uses = {SectorMapper.class, ClientTableStatusMapper.class})
public interface ClientTableMapper extends EntityMapper<ClientTableDTO, ClientTable> {

    @Mapping(source = "sector.id", target = "sectorId")
    @Mapping(source = "status.id", target = "statusId")
    ClientTableDTO toDto(ClientTable clientTable);

    @Mapping(source = "sectorId", target = "sector")
    @Mapping(source = "statusId", target = "status")
    ClientTable toEntity(ClientTableDTO clientTableDTO);

    default ClientTable fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientTable clientTable = new ClientTable();
        clientTable.setId(id);
        return clientTable;
    }
}
