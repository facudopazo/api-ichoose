package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.ClientTableStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientTableStatus and its DTO ClientTableStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientTableStatusMapper extends EntityMapper<ClientTableStatusDTO, ClientTableStatus> {



    default ClientTableStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientTableStatus clientTableStatus = new ClientTableStatus();
        clientTableStatus.setId(id);
        return clientTableStatus;
    }
}
