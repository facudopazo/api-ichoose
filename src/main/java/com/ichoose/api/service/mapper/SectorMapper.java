package com.ichoose.api.service.mapper;

import com.ichoose.api.domain.*;
import com.ichoose.api.service.dto.SectorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Sector and its DTO SectorDTO.
 */
@Mapper(componentModel = "spring", uses = {RestaurantMapper.class, SectorLocationMapper.class})
public interface SectorMapper extends EntityMapper<SectorDTO, Sector> {

    @Mapping(source = "restaurant.id", target = "restaurantId")
    @Mapping(source = "location.id", target = "locationId")
    SectorDTO toDto(Sector sector);

    @Mapping(source = "restaurantId", target = "restaurant")
    @Mapping(target = "tables", ignore = true)
    @Mapping(source = "locationId", target = "location")
    Sector toEntity(SectorDTO sectorDTO);

    default Sector fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sector sector = new Sector();
        sector.setId(id);
        return sector;
    }
}
