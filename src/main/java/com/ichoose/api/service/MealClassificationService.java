package com.ichoose.api.service;

import com.ichoose.api.domain.MealClassification;
import com.ichoose.api.repository.MealClassificationRepository;
import com.ichoose.api.repository.search.MealClassificationSearchRepository;
import com.ichoose.api.service.dto.MealClassificationDTO;
import com.ichoose.api.service.mapper.MealClassificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MealClassification.
 */
@Service
@Transactional
public class MealClassificationService {

    private final Logger log = LoggerFactory.getLogger(MealClassificationService.class);

    private final MealClassificationRepository mealClassificationRepository;

    private final MealClassificationMapper mealClassificationMapper;

    private final MealClassificationSearchRepository mealClassificationSearchRepository;

    public MealClassificationService(MealClassificationRepository mealClassificationRepository, MealClassificationMapper mealClassificationMapper, MealClassificationSearchRepository mealClassificationSearchRepository) {
        this.mealClassificationRepository = mealClassificationRepository;
        this.mealClassificationMapper = mealClassificationMapper;
        this.mealClassificationSearchRepository = mealClassificationSearchRepository;
    }

    /**
     * Save a mealClassification.
     *
     * @param mealClassificationDTO the entity to save
     * @return the persisted entity
     */
    public MealClassificationDTO save(MealClassificationDTO mealClassificationDTO) {
        log.debug("Request to save MealClassification : {}", mealClassificationDTO);
        MealClassification mealClassification = mealClassificationMapper.toEntity(mealClassificationDTO);
        mealClassification = mealClassificationRepository.save(mealClassification);
        MealClassificationDTO result = mealClassificationMapper.toDto(mealClassification);
        mealClassificationSearchRepository.save(mealClassification);
        return result;
    }

    /**
     * Get all the mealClassifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MealClassificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MealClassifications");
        return mealClassificationRepository.findAll(pageable)
            .map(mealClassificationMapper::toDto);
    }

    /**
     * Get one mealClassification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MealClassificationDTO findOne(Long id) {
        log.debug("Request to get MealClassification : {}", id);
        MealClassification mealClassification = mealClassificationRepository.findOne(id);
        return mealClassificationMapper.toDto(mealClassification);
    }

    /**
     * Delete the mealClassification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MealClassification : {}", id);
        mealClassificationRepository.delete(id);
        mealClassificationSearchRepository.delete(id);
    }

    /**
     * Search for the mealClassification corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MealClassificationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MealClassifications for query {}", query);
        Page<MealClassification> result = mealClassificationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(mealClassificationMapper::toDto);
    }
}
