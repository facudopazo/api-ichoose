package com.ichoose.api.service;

import com.ichoose.api.domain.RestaurantType;
import com.ichoose.api.repository.RestaurantTypeRepository;
import com.ichoose.api.repository.search.RestaurantTypeSearchRepository;
import com.ichoose.api.service.dto.RestaurantTypeDTO;
import com.ichoose.api.service.mapper.RestaurantTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RestaurantType.
 */
@Service
@Transactional
public class RestaurantTypeService {

    private final Logger log = LoggerFactory.getLogger(RestaurantTypeService.class);

    private final RestaurantTypeRepository restaurantTypeRepository;

    private final RestaurantTypeMapper restaurantTypeMapper;

    private final RestaurantTypeSearchRepository restaurantTypeSearchRepository;

    public RestaurantTypeService(RestaurantTypeRepository restaurantTypeRepository, RestaurantTypeMapper restaurantTypeMapper, RestaurantTypeSearchRepository restaurantTypeSearchRepository) {
        this.restaurantTypeRepository = restaurantTypeRepository;
        this.restaurantTypeMapper = restaurantTypeMapper;
        this.restaurantTypeSearchRepository = restaurantTypeSearchRepository;
    }

    /**
     * Save a restaurantType.
     *
     * @param restaurantTypeDTO the entity to save
     * @return the persisted entity
     */
    public RestaurantTypeDTO save(RestaurantTypeDTO restaurantTypeDTO) {
        log.debug("Request to save RestaurantType : {}", restaurantTypeDTO);
        RestaurantType restaurantType = restaurantTypeMapper.toEntity(restaurantTypeDTO);
        restaurantType = restaurantTypeRepository.save(restaurantType);
        RestaurantTypeDTO result = restaurantTypeMapper.toDto(restaurantType);
        restaurantTypeSearchRepository.save(restaurantType);
        return result;
    }

    /**
     * Get all the restaurantTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RestaurantTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RestaurantTypes");
        return restaurantTypeRepository.findAll(pageable)
            .map(restaurantTypeMapper::toDto);
    }

    /**
     * Get one restaurantType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RestaurantTypeDTO findOne(Long id) {
        log.debug("Request to get RestaurantType : {}", id);
        RestaurantType restaurantType = restaurantTypeRepository.findOne(id);
        return restaurantTypeMapper.toDto(restaurantType);
    }

    /**
     * Delete the restaurantType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RestaurantType : {}", id);
        restaurantTypeRepository.delete(id);
        restaurantTypeSearchRepository.delete(id);
    }

    /**
     * Search for the restaurantType corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RestaurantTypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RestaurantTypes for query {}", query);
        Page<RestaurantType> result = restaurantTypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(restaurantTypeMapper::toDto);
    }
}
