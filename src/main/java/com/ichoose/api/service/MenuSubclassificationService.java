package com.ichoose.api.service;

import com.ichoose.api.domain.MenuSubclassification;
import com.ichoose.api.repository.MenuSubclassificationRepository;
import com.ichoose.api.repository.search.MenuSubclassificationSearchRepository;
import com.ichoose.api.service.dto.MenuSubclassificationDTO;
import com.ichoose.api.service.mapper.MenuSubclassificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MenuSubclassification.
 */
@Service
@Transactional
public class MenuSubclassificationService {

    private final Logger log = LoggerFactory.getLogger(MenuSubclassificationService.class);

    private final MenuSubclassificationRepository menuSubclassificationRepository;

    private final MenuSubclassificationMapper menuSubclassificationMapper;

    private final MenuSubclassificationSearchRepository menuSubclassificationSearchRepository;

    public MenuSubclassificationService(MenuSubclassificationRepository menuSubclassificationRepository, MenuSubclassificationMapper menuSubclassificationMapper, MenuSubclassificationSearchRepository menuSubclassificationSearchRepository) {
        this.menuSubclassificationRepository = menuSubclassificationRepository;
        this.menuSubclassificationMapper = menuSubclassificationMapper;
        this.menuSubclassificationSearchRepository = menuSubclassificationSearchRepository;
    }

    /**
     * Save a menuSubclassification.
     *
     * @param menuSubclassificationDTO the entity to save
     * @return the persisted entity
     */
    public MenuSubclassificationDTO save(MenuSubclassificationDTO menuSubclassificationDTO) {
        log.debug("Request to save MenuSubclassification : {}", menuSubclassificationDTO);
        MenuSubclassification menuSubclassification = menuSubclassificationMapper.toEntity(menuSubclassificationDTO);
        menuSubclassification = menuSubclassificationRepository.save(menuSubclassification);
        MenuSubclassificationDTO result = menuSubclassificationMapper.toDto(menuSubclassification);
        menuSubclassificationSearchRepository.save(menuSubclassification);
        return result;
    }

    /**
     * Get all the menuSubclassifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuSubclassificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MenuSubclassifications");
        return menuSubclassificationRepository.findAll(pageable)
            .map(menuSubclassificationMapper::toDto);
    }

    /**
     * Get one menuSubclassification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MenuSubclassificationDTO findOne(Long id) {
        log.debug("Request to get MenuSubclassification : {}", id);
        MenuSubclassification menuSubclassification = menuSubclassificationRepository.findOne(id);
        return menuSubclassificationMapper.toDto(menuSubclassification);
    }

    /**
     * Delete the menuSubclassification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuSubclassification : {}", id);
        menuSubclassificationRepository.delete(id);
        menuSubclassificationSearchRepository.delete(id);
    }

    /**
     * Search for the menuSubclassification corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuSubclassificationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MenuSubclassifications for query {}", query);
        Page<MenuSubclassification> result = menuSubclassificationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(menuSubclassificationMapper::toDto);
    }
}
