package com.ichoose.api.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.ichoose.api.domain.Location;
import com.ichoose.api.repository.LocationRepository;
import com.ichoose.api.repository.search.LocationSearchRepository;
import com.ichoose.api.service.dto.LocationDTO;
import com.ichoose.api.service.mapper.LocationMapper;

/**
 * Service Implementation for managing Location.
 */
@Service
@Transactional
public class LocationService {

    private final Logger log = LoggerFactory.getLogger(LocationService.class);

    private final LocationRepository locationRepository;

    private final LocationMapper locationMapper;

    private final LocationSearchRepository locationSearchRepository;

    public LocationService(LocationRepository locationRepository, LocationMapper locationMapper, LocationSearchRepository locationSearchRepository) {
        this.locationRepository = locationRepository;
        this.locationMapper = locationMapper;
        this.locationSearchRepository = locationSearchRepository;
    }

    /**
     * Save a location.
     *
     * @param locationDTO the entity to save
     * @return the persisted entity
     */
    public LocationDTO save(LocationDTO locationDTO) {
        log.debug("Request to save Location : {}", locationDTO);
        Location location = locationMapper.toEntity(locationDTO);
        location = locationRepository.save(location);
        LocationDTO result = locationMapper.toDto(location);
        locationSearchRepository.save(location);
        return result;
    }

    /**
     * Get all the locations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LocationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Locations");
        return locationRepository.findAll(pageable)
            .map(locationMapper::toDto);
    }

    /**
     * Get one location by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public LocationDTO findOne(Long id) {
        log.debug("Request to get Location : {}", id);
        Location location = locationRepository.findOne(id);
        return locationMapper.toDto(location);
    }

    /**
     * Delete the location by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Location : {}", id);
        locationRepository.delete(id);
        locationSearchRepository.delete(id);
    }

    /**
     * Search for the location corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LocationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Locations for query {}", query);
        Page<Location> result = locationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(locationMapper::toDto);
    }
    
    /**
     * Find the location using Google API, by the given latitude and longitude.
     * Create the Location object and return it.
     * The location must be an address. If not, return null.
     * 
     * @param latitude
     * @param longitude
     * @return the founded location
     */
    public Location findByLatitudeAndLongitude(Double latitude, Double longitude){
    	GeoApiContext context = new GeoApiContext.Builder()
    		    .apiKey("AIzaSyCjI-UqdBc1tmRNsR0fWBDrt7siOnmFSas")
    		    .build();
			try {				
				List<GeocodingResult> results = Arrays.asList(GeocodingApi.reverseGeocode(context, new LatLng(latitude, longitude)).await());
				Optional<GeocodingResult> theResult = results.stream()
						.filter(result -> Arrays.asList(result.types).stream().anyMatch(type -> type.equals(AddressType.STREET_ADDRESS)))
						.findFirst();
				if(theResult.isPresent()){
					Optional<AddressComponent> postalCodeAddressComponent = Arrays.asList(theResult.get().addressComponents).stream()
							.filter(component -> Arrays.asList(component.types).stream().anyMatch(type -> type.equals(AddressComponentType.POSTAL_CODE)))
							.findFirst();
					Optional<AddressComponent> routeAddressComponent = Arrays.asList(theResult.get().addressComponents).stream()
							.filter(component -> Arrays.asList(component.types).stream().anyMatch(type -> type.equals(AddressComponentType.ROUTE)))
							.findFirst();
					Optional<AddressComponent> streetNumberAddressComponent = Arrays.asList(theResult.get().addressComponents).stream()
							.filter(component -> Arrays.asList(component.types).stream().anyMatch(type -> type.equals(AddressComponentType.STREET_NUMBER)))
							.findFirst();
					Optional<AddressComponent> countryAddressComponent = Arrays.asList(theResult.get().addressComponents).stream()
							.filter(component -> Arrays.asList(component.types).stream().anyMatch(type -> type.equals(AddressComponentType.COUNTRY)))
							.findFirst();
					String formattedAddress = theResult.get().formattedAddress;
					Location location = new Location()
							.country(countryAddressComponent.get().longName)
							.formattedAddress(formattedAddress)
							.latitude(latitude)
							.longitude(longitude)
							.postalCode(postalCodeAddressComponent.get().longName)
							.streetNumber(streetNumberAddressComponent.get().longName)
							.route(routeAddressComponent.get().longName);
					location = locationRepository.save(location);
					return location;
				}
				return null;


			} catch (ApiException | InterruptedException | IOException e) {
				e.printStackTrace();
				return null;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
    }
}
