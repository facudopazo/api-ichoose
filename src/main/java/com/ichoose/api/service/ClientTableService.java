package com.ichoose.api.service;

import com.ichoose.api.domain.ClientTable;
import com.ichoose.api.repository.ClientTableRepository;
import com.ichoose.api.repository.search.ClientTableSearchRepository;
import com.ichoose.api.service.dto.ClientTableDTO;
import com.ichoose.api.service.mapper.ClientTableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ClientTable.
 */
@Service
@Transactional
public class ClientTableService {

    private final Logger log = LoggerFactory.getLogger(ClientTableService.class);

    private final ClientTableRepository clientTableRepository;

    private final ClientTableMapper clientTableMapper;

    private final ClientTableSearchRepository clientTableSearchRepository;

    public ClientTableService(ClientTableRepository clientTableRepository, ClientTableMapper clientTableMapper, ClientTableSearchRepository clientTableSearchRepository) {
        this.clientTableRepository = clientTableRepository;
        this.clientTableMapper = clientTableMapper;
        this.clientTableSearchRepository = clientTableSearchRepository;
    }

    /**
     * Save a clientTable.
     *
     * @param clientTableDTO the entity to save
     * @return the persisted entity
     */
    public ClientTableDTO save(ClientTableDTO clientTableDTO) {
        log.debug("Request to save ClientTable : {}", clientTableDTO);
        ClientTable clientTable = clientTableMapper.toEntity(clientTableDTO);
        clientTable = clientTableRepository.save(clientTable);
        ClientTableDTO result = clientTableMapper.toDto(clientTable);
        clientTableSearchRepository.save(clientTable);
        return result;
    }

    /**
     * Get all the clientTables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientTableDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientTables");
        return clientTableRepository.findAll(pageable)
            .map(clientTableMapper::toDto);
    }

    /**
     * Get one clientTable by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientTableDTO findOne(Long id) {
        log.debug("Request to get ClientTable : {}", id);
        ClientTable clientTable = clientTableRepository.findOne(id);
        return clientTableMapper.toDto(clientTable);
    }

    /**
     * Delete the clientTable by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientTable : {}", id);
        clientTableRepository.delete(id);
        clientTableSearchRepository.delete(id);
    }

    /**
     * Search for the clientTable corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientTableDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ClientTables for query {}", query);
        Page<ClientTable> result = clientTableSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(clientTableMapper::toDto);
    }
}
