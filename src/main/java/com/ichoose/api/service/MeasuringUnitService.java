package com.ichoose.api.service;

import com.ichoose.api.domain.MeasuringUnit;
import com.ichoose.api.repository.MeasuringUnitRepository;
import com.ichoose.api.repository.search.MeasuringUnitSearchRepository;
import com.ichoose.api.service.dto.MeasuringUnitDTO;
import com.ichoose.api.service.mapper.MeasuringUnitMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MeasuringUnit.
 */
@Service
@Transactional
public class MeasuringUnitService {

    private final Logger log = LoggerFactory.getLogger(MeasuringUnitService.class);

    private final MeasuringUnitRepository measuringUnitRepository;

    private final MeasuringUnitMapper measuringUnitMapper;

    private final MeasuringUnitSearchRepository measuringUnitSearchRepository;

    public MeasuringUnitService(MeasuringUnitRepository measuringUnitRepository, MeasuringUnitMapper measuringUnitMapper, MeasuringUnitSearchRepository measuringUnitSearchRepository) {
        this.measuringUnitRepository = measuringUnitRepository;
        this.measuringUnitMapper = measuringUnitMapper;
        this.measuringUnitSearchRepository = measuringUnitSearchRepository;
    }

    /**
     * Save a measuringUnit.
     *
     * @param measuringUnitDTO the entity to save
     * @return the persisted entity
     */
    public MeasuringUnitDTO save(MeasuringUnitDTO measuringUnitDTO) {
        log.debug("Request to save MeasuringUnit : {}", measuringUnitDTO);
        MeasuringUnit measuringUnit = measuringUnitMapper.toEntity(measuringUnitDTO);
        measuringUnit = measuringUnitRepository.save(measuringUnit);
        MeasuringUnitDTO result = measuringUnitMapper.toDto(measuringUnit);
        measuringUnitSearchRepository.save(measuringUnit);
        return result;
    }

    /**
     * Get all the measuringUnits.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MeasuringUnitDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MeasuringUnits");
        return measuringUnitRepository.findAll(pageable)
            .map(measuringUnitMapper::toDto);
    }

    /**
     * Get one measuringUnit by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MeasuringUnitDTO findOne(Long id) {
        log.debug("Request to get MeasuringUnit : {}", id);
        MeasuringUnit measuringUnit = measuringUnitRepository.findOne(id);
        return measuringUnitMapper.toDto(measuringUnit);
    }

    /**
     * Delete the measuringUnit by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MeasuringUnit : {}", id);
        measuringUnitRepository.delete(id);
        measuringUnitSearchRepository.delete(id);
    }

    /**
     * Search for the measuringUnit corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MeasuringUnitDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MeasuringUnits for query {}", query);
        Page<MeasuringUnit> result = measuringUnitSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(measuringUnitMapper::toDto);
    }
}
