package com.ichoose.api.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.ichoose.api.domain.MeasuringUnit;
import com.ichoose.api.domain.MenuClassification;
import com.ichoose.api.domain.enumeration.DestinationType;

/**
 * A DTO for the Menu entity.
 */
public class MenuDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Double price;

    @NotNull
    private Double quantity;

    private DestinationType destination;

    private Boolean disabled;

    private ZonedDateTime deleteDate;

    private Long measuringUnitId;
    
    private MeasuringUnit measuringUnit;

    private Long classificationId;
    
    private MenuClassification classification;

    private Long restaurantId;

    private Set<PictureDTO> pictures = new HashSet<>();

    private Set<MenuSubclassificationDTO> subclassifications = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public DestinationType getDestination() {
        return destination;
    }

    public void setDestination(DestinationType destination) {
        this.destination = destination;
    }

    public Boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public ZonedDateTime getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Long getMeasuringUnitId() {
        return measuringUnitId;
    }

    public void setMeasuringUnitId(Long measuringUnitId) {
        this.measuringUnitId = measuringUnitId;
    }

    public MeasuringUnit getMeasuringUnit() {
		return measuringUnit;
	}

	public void setMeasuringUnit(MeasuringUnit measuringUnit) {
		this.measuringUnit = measuringUnit;
	}

	public Long getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(Long menuClassificationId) {
        this.classificationId = menuClassificationId;
    }

    public MenuClassification getClassification() {
		return classification;
	}

	public void setClassification(MenuClassification menuClassification) {
		this.classification = menuClassification;
	}

	public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Set<PictureDTO> getPictures() {
        return pictures;
    }

    public void setPictures(Set<PictureDTO> pictures) {
        this.pictures = pictures;
    }

    public Set<MenuSubclassificationDTO> getSubclassifications() {
        return subclassifications;
    }

    public void setSubclassifications(Set<MenuSubclassificationDTO> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MenuDTO menuDTO = (MenuDTO) o;
        if(menuDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", price=" + getPrice() +
            ", quantity=" + getQuantity() +
            ", destination='" + getDestination() + "'" +
            ", disabled='" + isDisabled() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
