package com.ichoose.api.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ClientTable entity.
 */
public class ClientTableDTO implements Serializable {

    private Long id;

    @NotNull
    private String number;

    @NotNull
    private Integer chairs;

    @NotNull
    private Boolean modifiable;

    private ZonedDateTime deleteDate;

    private Long sectorId;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getChairs() {
        return chairs;
    }

    public void setChairs(Integer chairs) {
        this.chairs = chairs;
    }

    public Boolean isModifiable() {
        return modifiable;
    }

    public void setModifiable(Boolean modifiable) {
        this.modifiable = modifiable;
    }

    public ZonedDateTime getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(ZonedDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Long getSectorId() {
        return sectorId;
    }

    public void setSectorId(Long sectorId) {
        this.sectorId = sectorId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long clientTableStatusId) {
        this.statusId = clientTableStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientTableDTO clientTableDTO = (ClientTableDTO) o;
        if(clientTableDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientTableDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientTableDTO{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", chairs=" + getChairs() +
            ", modifiable='" + isModifiable() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
