package com.ichoose.api.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the MenuClassification entity.
 */
public class MenuClassificationDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Set<MenuSubclassificationDTO> subclassifications = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MenuSubclassificationDTO> getSubclassifications() {
        return subclassifications;
    }

    public void setSubclassifications(Set<MenuSubclassificationDTO> menuSubclassifications) {
        this.subclassifications = menuSubclassifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MenuClassificationDTO menuClassificationDTO = (MenuClassificationDTO) o;
        if(menuClassificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuClassificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuClassificationDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
