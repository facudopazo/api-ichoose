package com.ichoose.api.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Restaurant entity.
 */
public class RestaurantDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private String cuit;

    private String cbu;

    private Long locationId;
    
    private LocationDTO location;

    private Long restaurantTypeId;
    
    private RestaurantTypeDTO restaurantType;

    private Set<MealClassificationDTO> mealClassifications = new HashSet<>();
    
    private Set<PictureDTO> pictures = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public LocationDTO getLocation() {
		return location;
	}

	public void setLocation(LocationDTO location) {
		this.location = location;
	}

	public Long getRestaurantTypeId() {
        return restaurantTypeId;
    }

    public void setRestaurantTypeId(Long restaurantTypeId) {
        this.restaurantTypeId = restaurantTypeId;
    }

    public RestaurantTypeDTO getRestaurantType() {
		return restaurantType;
	}

	public void setRestaurantType(RestaurantTypeDTO restaurantType) {
		this.restaurantType = restaurantType;
	}

	public Set<MealClassificationDTO> getMealClassifications() {
        return mealClassifications;
    }

    public void setMealClassifications(Set<MealClassificationDTO> mealClassifications) {
        this.mealClassifications = mealClassifications;
    }

    public Set<PictureDTO> getPictures() {
		return pictures;
	}

	public void setPictures(Set<PictureDTO> pictures) {
		this.pictures = pictures;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RestaurantDTO restaurantDTO = (RestaurantDTO) o;
        if(restaurantDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), restaurantDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RestaurantDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", cuit='" + getCuit() + "'" +
            ", cbu='" + getCbu() + "'" +
            "}";
    }
}
