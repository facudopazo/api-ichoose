package com.ichoose.api.service;

import com.ichoose.api.domain.MenuClassification;
import com.ichoose.api.repository.MenuClassificationRepository;
import com.ichoose.api.repository.search.MenuClassificationSearchRepository;
import com.ichoose.api.service.dto.MenuClassificationDTO;
import com.ichoose.api.service.mapper.MenuClassificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MenuClassification.
 */
@Service
@Transactional
public class MenuClassificationService {

    private final Logger log = LoggerFactory.getLogger(MenuClassificationService.class);

    private final MenuClassificationRepository menuClassificationRepository;

    private final MenuClassificationMapper menuClassificationMapper;

    private final MenuClassificationSearchRepository menuClassificationSearchRepository;

    public MenuClassificationService(MenuClassificationRepository menuClassificationRepository, MenuClassificationMapper menuClassificationMapper, MenuClassificationSearchRepository menuClassificationSearchRepository) {
        this.menuClassificationRepository = menuClassificationRepository;
        this.menuClassificationMapper = menuClassificationMapper;
        this.menuClassificationSearchRepository = menuClassificationSearchRepository;
    }

    /**
     * Save a menuClassification.
     *
     * @param menuClassificationDTO the entity to save
     * @return the persisted entity
     */
    public MenuClassificationDTO save(MenuClassificationDTO menuClassificationDTO) {
        log.debug("Request to save MenuClassification : {}", menuClassificationDTO);
        MenuClassification menuClassification = menuClassificationMapper.toEntity(menuClassificationDTO);
        menuClassification = menuClassificationRepository.save(menuClassification);
        MenuClassificationDTO result = menuClassificationMapper.toDto(menuClassification);
        menuClassificationSearchRepository.save(menuClassification);
        return result;
    }

    /**
     * Get all the menuClassifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuClassificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MenuClassifications");
        return menuClassificationRepository.findAll(pageable)
            .map(menuClassificationMapper::toDto);
    }

    /**
     * Get one menuClassification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MenuClassificationDTO findOne(Long id) {
        log.debug("Request to get MenuClassification : {}", id);
        MenuClassification menuClassification = menuClassificationRepository.findOneWithEagerRelationships(id);
        return menuClassificationMapper.toDto(menuClassification);
    }

    /**
     * Delete the menuClassification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuClassification : {}", id);
        menuClassificationRepository.delete(id);
        menuClassificationSearchRepository.delete(id);
    }

    /**
     * Search for the menuClassification corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuClassificationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MenuClassifications for query {}", query);
        Page<MenuClassification> result = menuClassificationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(menuClassificationMapper::toDto);
    }
}
