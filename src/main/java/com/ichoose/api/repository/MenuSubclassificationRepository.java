package com.ichoose.api.repository;

import com.ichoose.api.domain.MenuSubclassification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MenuSubclassification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuSubclassificationRepository extends JpaRepository<MenuSubclassification, Long> {

}
