package com.ichoose.api.repository;

import com.ichoose.api.domain.MealClassification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MealClassification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MealClassificationRepository extends JpaRepository<MealClassification, Long> {

}
