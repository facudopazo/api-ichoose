package com.ichoose.api.repository;

import com.ichoose.api.domain.Restaurant;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Restaurant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    @Query("select distinct restaurant from Restaurant restaurant left join fetch restaurant.mealClassifications")
    List<Restaurant> findAllWithEagerRelationships();

    @Query("select restaurant from Restaurant restaurant left join fetch restaurant.mealClassifications where restaurant.id =:id")
    Restaurant findOneWithEagerRelationships(@Param("id") Long id);

}
