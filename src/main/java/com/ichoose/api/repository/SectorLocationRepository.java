package com.ichoose.api.repository;

import com.ichoose.api.domain.SectorLocation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SectorLocation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SectorLocationRepository extends JpaRepository<SectorLocation, Long> {

}
