package com.ichoose.api.repository;

import com.ichoose.api.domain.MeasuringUnit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MeasuringUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeasuringUnitRepository extends JpaRepository<MeasuringUnit, Long> {

}
