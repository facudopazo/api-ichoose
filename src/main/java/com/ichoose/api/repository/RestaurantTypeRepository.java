package com.ichoose.api.repository;

import com.ichoose.api.domain.RestaurantType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RestaurantType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RestaurantTypeRepository extends JpaRepository<RestaurantType, Long> {

}
