package com.ichoose.api.repository;

import com.ichoose.api.domain.MenuClassification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the MenuClassification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuClassificationRepository extends JpaRepository<MenuClassification, Long> {
    @Query("select distinct menu_classification from MenuClassification menu_classification left join fetch menu_classification.subclassifications")
    List<MenuClassification> findAllWithEagerRelationships();

    @Query("select menu_classification from MenuClassification menu_classification left join fetch menu_classification.subclassifications where menu_classification.id =:id")
    MenuClassification findOneWithEagerRelationships(@Param("id") Long id);

}
