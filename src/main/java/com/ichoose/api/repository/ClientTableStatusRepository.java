package com.ichoose.api.repository;

import com.ichoose.api.domain.ClientTableStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientTableStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientTableStatusRepository extends JpaRepository<ClientTableStatus, Long> {

}
