package com.ichoose.api.repository;

import com.ichoose.api.domain.ClientTable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientTable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientTableRepository extends JpaRepository<ClientTable, Long> {

}
