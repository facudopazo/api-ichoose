package com.ichoose.api.repository.search;

import com.ichoose.api.domain.MeasuringUnit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MeasuringUnit entity.
 */
public interface MeasuringUnitSearchRepository extends ElasticsearchRepository<MeasuringUnit, Long> {
}
