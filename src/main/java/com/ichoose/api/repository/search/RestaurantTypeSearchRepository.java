package com.ichoose.api.repository.search;

import com.ichoose.api.domain.RestaurantType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RestaurantType entity.
 */
public interface RestaurantTypeSearchRepository extends ElasticsearchRepository<RestaurantType, Long> {
}
