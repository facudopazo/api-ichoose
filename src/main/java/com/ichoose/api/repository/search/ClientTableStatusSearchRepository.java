package com.ichoose.api.repository.search;

import com.ichoose.api.domain.ClientTableStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientTableStatus entity.
 */
public interface ClientTableStatusSearchRepository extends ElasticsearchRepository<ClientTableStatus, Long> {
}
