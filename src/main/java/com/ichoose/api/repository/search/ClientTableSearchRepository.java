package com.ichoose.api.repository.search;

import com.ichoose.api.domain.ClientTable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientTable entity.
 */
public interface ClientTableSearchRepository extends ElasticsearchRepository<ClientTable, Long> {
}
