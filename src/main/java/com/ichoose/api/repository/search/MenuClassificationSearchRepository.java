package com.ichoose.api.repository.search;

import com.ichoose.api.domain.MenuClassification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MenuClassification entity.
 */
public interface MenuClassificationSearchRepository extends ElasticsearchRepository<MenuClassification, Long> {
}
