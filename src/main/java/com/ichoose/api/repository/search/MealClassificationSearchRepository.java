package com.ichoose.api.repository.search;

import com.ichoose.api.domain.MealClassification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MealClassification entity.
 */
public interface MealClassificationSearchRepository extends ElasticsearchRepository<MealClassification, Long> {
}
