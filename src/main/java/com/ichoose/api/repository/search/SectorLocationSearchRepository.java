package com.ichoose.api.repository.search;

import com.ichoose.api.domain.SectorLocation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SectorLocation entity.
 */
public interface SectorLocationSearchRepository extends ElasticsearchRepository<SectorLocation, Long> {
}
