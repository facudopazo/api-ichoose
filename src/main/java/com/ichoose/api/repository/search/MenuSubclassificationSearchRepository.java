package com.ichoose.api.repository.search;

import com.ichoose.api.domain.MenuSubclassification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MenuSubclassification entity.
 */
public interface MenuSubclassificationSearchRepository extends ElasticsearchRepository<MenuSubclassification, Long> {
}
