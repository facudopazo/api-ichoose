package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.MeasuringUnit;
import com.ichoose.api.repository.MeasuringUnitRepository;
import com.ichoose.api.service.MeasuringUnitService;
import com.ichoose.api.repository.search.MeasuringUnitSearchRepository;
import com.ichoose.api.service.dto.MeasuringUnitDTO;
import com.ichoose.api.service.mapper.MeasuringUnitMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MeasuringUnitResource REST controller.
 *
 * @see MeasuringUnitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class MeasuringUnitResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private MeasuringUnitRepository measuringUnitRepository;

    @Autowired
    private MeasuringUnitMapper measuringUnitMapper;

    @Autowired
    private MeasuringUnitService measuringUnitService;

    @Autowired
    private MeasuringUnitSearchRepository measuringUnitSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMeasuringUnitMockMvc;

    private MeasuringUnit measuringUnit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MeasuringUnitResource measuringUnitResource = new MeasuringUnitResource(measuringUnitService);
        this.restMeasuringUnitMockMvc = MockMvcBuilders.standaloneSetup(measuringUnitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeasuringUnit createEntity(EntityManager em) {
        MeasuringUnit measuringUnit = new MeasuringUnit()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE);
        return measuringUnit;
    }

    @Before
    public void initTest() {
        measuringUnitSearchRepository.deleteAll();
        measuringUnit = createEntity(em);
    }

    @Test
    @Transactional
    public void createMeasuringUnit() throws Exception {
        int databaseSizeBeforeCreate = measuringUnitRepository.findAll().size();

        // Create the MeasuringUnit
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(measuringUnit);
        restMeasuringUnitMockMvc.perform(post("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isCreated());

        // Validate the MeasuringUnit in the database
        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeCreate + 1);
        MeasuringUnit testMeasuringUnit = measuringUnitList.get(measuringUnitList.size() - 1);
        assertThat(testMeasuringUnit.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMeasuringUnit.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the MeasuringUnit in Elasticsearch
        MeasuringUnit measuringUnitEs = measuringUnitSearchRepository.findOne(testMeasuringUnit.getId());
        assertThat(measuringUnitEs).isEqualToIgnoringGivenFields(testMeasuringUnit);
    }

    @Test
    @Transactional
    public void createMeasuringUnitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = measuringUnitRepository.findAll().size();

        // Create the MeasuringUnit with an existing ID
        measuringUnit.setId(1L);
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(measuringUnit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeasuringUnitMockMvc.perform(post("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MeasuringUnit in the database
        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = measuringUnitRepository.findAll().size();
        // set the field null
        measuringUnit.setName(null);

        // Create the MeasuringUnit, which fails.
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(measuringUnit);

        restMeasuringUnitMockMvc.perform(post("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isBadRequest());

        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = measuringUnitRepository.findAll().size();
        // set the field null
        measuringUnit.setCode(null);

        // Create the MeasuringUnit, which fails.
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(measuringUnit);

        restMeasuringUnitMockMvc.perform(post("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isBadRequest());

        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMeasuringUnits() throws Exception {
        // Initialize the database
        measuringUnitRepository.saveAndFlush(measuringUnit);

        // Get all the measuringUnitList
        restMeasuringUnitMockMvc.perform(get("/api/measuring-units?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(measuringUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getMeasuringUnit() throws Exception {
        // Initialize the database
        measuringUnitRepository.saveAndFlush(measuringUnit);

        // Get the measuringUnit
        restMeasuringUnitMockMvc.perform(get("/api/measuring-units/{id}", measuringUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(measuringUnit.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMeasuringUnit() throws Exception {
        // Get the measuringUnit
        restMeasuringUnitMockMvc.perform(get("/api/measuring-units/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMeasuringUnit() throws Exception {
        // Initialize the database
        measuringUnitRepository.saveAndFlush(measuringUnit);
        measuringUnitSearchRepository.save(measuringUnit);
        int databaseSizeBeforeUpdate = measuringUnitRepository.findAll().size();

        // Update the measuringUnit
        MeasuringUnit updatedMeasuringUnit = measuringUnitRepository.findOne(measuringUnit.getId());
        // Disconnect from session so that the updates on updatedMeasuringUnit are not directly saved in db
        em.detach(updatedMeasuringUnit);
        updatedMeasuringUnit
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(updatedMeasuringUnit);

        restMeasuringUnitMockMvc.perform(put("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isOk());

        // Validate the MeasuringUnit in the database
        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeUpdate);
        MeasuringUnit testMeasuringUnit = measuringUnitList.get(measuringUnitList.size() - 1);
        assertThat(testMeasuringUnit.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMeasuringUnit.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the MeasuringUnit in Elasticsearch
        MeasuringUnit measuringUnitEs = measuringUnitSearchRepository.findOne(testMeasuringUnit.getId());
        assertThat(measuringUnitEs).isEqualToIgnoringGivenFields(testMeasuringUnit);
    }

    @Test
    @Transactional
    public void updateNonExistingMeasuringUnit() throws Exception {
        int databaseSizeBeforeUpdate = measuringUnitRepository.findAll().size();

        // Create the MeasuringUnit
        MeasuringUnitDTO measuringUnitDTO = measuringUnitMapper.toDto(measuringUnit);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMeasuringUnitMockMvc.perform(put("/api/measuring-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(measuringUnitDTO)))
            .andExpect(status().isCreated());

        // Validate the MeasuringUnit in the database
        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMeasuringUnit() throws Exception {
        // Initialize the database
        measuringUnitRepository.saveAndFlush(measuringUnit);
        measuringUnitSearchRepository.save(measuringUnit);
        int databaseSizeBeforeDelete = measuringUnitRepository.findAll().size();

        // Get the measuringUnit
        restMeasuringUnitMockMvc.perform(delete("/api/measuring-units/{id}", measuringUnit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean measuringUnitExistsInEs = measuringUnitSearchRepository.exists(measuringUnit.getId());
        assertThat(measuringUnitExistsInEs).isFalse();

        // Validate the database is empty
        List<MeasuringUnit> measuringUnitList = measuringUnitRepository.findAll();
        assertThat(measuringUnitList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMeasuringUnit() throws Exception {
        // Initialize the database
        measuringUnitRepository.saveAndFlush(measuringUnit);
        measuringUnitSearchRepository.save(measuringUnit);

        // Search the measuringUnit
        restMeasuringUnitMockMvc.perform(get("/api/_search/measuring-units?query=id:" + measuringUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(measuringUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeasuringUnit.class);
        MeasuringUnit measuringUnit1 = new MeasuringUnit();
        measuringUnit1.setId(1L);
        MeasuringUnit measuringUnit2 = new MeasuringUnit();
        measuringUnit2.setId(measuringUnit1.getId());
        assertThat(measuringUnit1).isEqualTo(measuringUnit2);
        measuringUnit2.setId(2L);
        assertThat(measuringUnit1).isNotEqualTo(measuringUnit2);
        measuringUnit1.setId(null);
        assertThat(measuringUnit1).isNotEqualTo(measuringUnit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeasuringUnitDTO.class);
        MeasuringUnitDTO measuringUnitDTO1 = new MeasuringUnitDTO();
        measuringUnitDTO1.setId(1L);
        MeasuringUnitDTO measuringUnitDTO2 = new MeasuringUnitDTO();
        assertThat(measuringUnitDTO1).isNotEqualTo(measuringUnitDTO2);
        measuringUnitDTO2.setId(measuringUnitDTO1.getId());
        assertThat(measuringUnitDTO1).isEqualTo(measuringUnitDTO2);
        measuringUnitDTO2.setId(2L);
        assertThat(measuringUnitDTO1).isNotEqualTo(measuringUnitDTO2);
        measuringUnitDTO1.setId(null);
        assertThat(measuringUnitDTO1).isNotEqualTo(measuringUnitDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(measuringUnitMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(measuringUnitMapper.fromId(null)).isNull();
    }
}
