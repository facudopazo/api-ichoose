package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.ClientTable;
import com.ichoose.api.repository.ClientTableRepository;
import com.ichoose.api.service.ClientTableService;
import com.ichoose.api.repository.search.ClientTableSearchRepository;
import com.ichoose.api.service.dto.ClientTableDTO;
import com.ichoose.api.service.mapper.ClientTableMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.sameInstant;
import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientTableResource REST controller.
 *
 * @see ClientTableResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class ClientTableResourceIntTest {

    private static final String DEFAULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_CHAIRS = 1;
    private static final Integer UPDATED_CHAIRS = 2;

    private static final Boolean DEFAULT_MODIFIABLE = false;
    private static final Boolean UPDATED_MODIFIABLE = true;

    private static final ZonedDateTime DEFAULT_DELETE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ClientTableRepository clientTableRepository;

    @Autowired
    private ClientTableMapper clientTableMapper;

    @Autowired
    private ClientTableService clientTableService;

    @Autowired
    private ClientTableSearchRepository clientTableSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientTableMockMvc;

    private ClientTable clientTable;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientTableResource clientTableResource = new ClientTableResource(clientTableService);
        this.restClientTableMockMvc = MockMvcBuilders.standaloneSetup(clientTableResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientTable createEntity(EntityManager em) {
        ClientTable clientTable = new ClientTable()
            .number(DEFAULT_NUMBER)
            .chairs(DEFAULT_CHAIRS)
            .modifiable(DEFAULT_MODIFIABLE)
            .deleteDate(DEFAULT_DELETE_DATE);
        return clientTable;
    }

    @Before
    public void initTest() {
        clientTableSearchRepository.deleteAll();
        clientTable = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientTable() throws Exception {
        int databaseSizeBeforeCreate = clientTableRepository.findAll().size();

        // Create the ClientTable
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);
        restClientTableMockMvc.perform(post("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientTable in the database
        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeCreate + 1);
        ClientTable testClientTable = clientTableList.get(clientTableList.size() - 1);
        assertThat(testClientTable.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testClientTable.getChairs()).isEqualTo(DEFAULT_CHAIRS);
        assertThat(testClientTable.isModifiable()).isEqualTo(DEFAULT_MODIFIABLE);
        assertThat(testClientTable.getDeleteDate()).isEqualTo(DEFAULT_DELETE_DATE);

        // Validate the ClientTable in Elasticsearch
        ClientTable clientTableEs = clientTableSearchRepository.findOne(testClientTable.getId());
        assertThat(testClientTable.getDeleteDate()).isEqualTo(testClientTable.getDeleteDate());
        assertThat(clientTableEs).isEqualToIgnoringGivenFields(testClientTable, "deleteDate");
    }

    @Test
    @Transactional
    public void createClientTableWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientTableRepository.findAll().size();

        // Create the ClientTable with an existing ID
        clientTable.setId(1L);
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientTableMockMvc.perform(post("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientTable in the database
        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientTableRepository.findAll().size();
        // set the field null
        clientTable.setNumber(null);

        // Create the ClientTable, which fails.
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);

        restClientTableMockMvc.perform(post("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isBadRequest());

        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChairsIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientTableRepository.findAll().size();
        // set the field null
        clientTable.setChairs(null);

        // Create the ClientTable, which fails.
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);

        restClientTableMockMvc.perform(post("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isBadRequest());

        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiableIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientTableRepository.findAll().size();
        // set the field null
        clientTable.setModifiable(null);

        // Create the ClientTable, which fails.
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);

        restClientTableMockMvc.perform(post("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isBadRequest());

        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientTables() throws Exception {
        // Initialize the database
        clientTableRepository.saveAndFlush(clientTable);

        // Get all the clientTableList
        restClientTableMockMvc.perform(get("/api/client-tables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientTable.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].chairs").value(hasItem(DEFAULT_CHAIRS)))
            .andExpect(jsonPath("$.[*].modifiable").value(hasItem(DEFAULT_MODIFIABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].deleteDate").value(hasItem(sameInstant(DEFAULT_DELETE_DATE))));
    }

    @Test
    @Transactional
    public void getClientTable() throws Exception {
        // Initialize the database
        clientTableRepository.saveAndFlush(clientTable);

        // Get the clientTable
        restClientTableMockMvc.perform(get("/api/client-tables/{id}", clientTable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientTable.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()))
            .andExpect(jsonPath("$.chairs").value(DEFAULT_CHAIRS))
            .andExpect(jsonPath("$.modifiable").value(DEFAULT_MODIFIABLE.booleanValue()))
            .andExpect(jsonPath("$.deleteDate").value(sameInstant(DEFAULT_DELETE_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingClientTable() throws Exception {
        // Get the clientTable
        restClientTableMockMvc.perform(get("/api/client-tables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientTable() throws Exception {
        // Initialize the database
        clientTableRepository.saveAndFlush(clientTable);
        clientTableSearchRepository.save(clientTable);
        int databaseSizeBeforeUpdate = clientTableRepository.findAll().size();

        // Update the clientTable
        ClientTable updatedClientTable = clientTableRepository.findOne(clientTable.getId());
        // Disconnect from session so that the updates on updatedClientTable are not directly saved in db
        em.detach(updatedClientTable);
        updatedClientTable
            .number(UPDATED_NUMBER)
            .chairs(UPDATED_CHAIRS)
            .modifiable(UPDATED_MODIFIABLE)
            .deleteDate(UPDATED_DELETE_DATE);
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(updatedClientTable);

        restClientTableMockMvc.perform(put("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isOk());

        // Validate the ClientTable in the database
        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeUpdate);
        ClientTable testClientTable = clientTableList.get(clientTableList.size() - 1);
        assertThat(testClientTable.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testClientTable.getChairs()).isEqualTo(UPDATED_CHAIRS);
        assertThat(testClientTable.isModifiable()).isEqualTo(UPDATED_MODIFIABLE);
        assertThat(testClientTable.getDeleteDate()).isEqualTo(UPDATED_DELETE_DATE);

        // Validate the ClientTable in Elasticsearch
        ClientTable clientTableEs = clientTableSearchRepository.findOne(testClientTable.getId());
        assertThat(testClientTable.getDeleteDate()).isEqualTo(testClientTable.getDeleteDate());
        assertThat(clientTableEs).isEqualToIgnoringGivenFields(testClientTable, "deleteDate");
    }

    @Test
    @Transactional
    public void updateNonExistingClientTable() throws Exception {
        int databaseSizeBeforeUpdate = clientTableRepository.findAll().size();

        // Create the ClientTable
        ClientTableDTO clientTableDTO = clientTableMapper.toDto(clientTable);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientTableMockMvc.perform(put("/api/client-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientTable in the database
        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientTable() throws Exception {
        // Initialize the database
        clientTableRepository.saveAndFlush(clientTable);
        clientTableSearchRepository.save(clientTable);
        int databaseSizeBeforeDelete = clientTableRepository.findAll().size();

        // Get the clientTable
        restClientTableMockMvc.perform(delete("/api/client-tables/{id}", clientTable.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clientTableExistsInEs = clientTableSearchRepository.exists(clientTable.getId());
        assertThat(clientTableExistsInEs).isFalse();

        // Validate the database is empty
        List<ClientTable> clientTableList = clientTableRepository.findAll();
        assertThat(clientTableList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClientTable() throws Exception {
        // Initialize the database
        clientTableRepository.saveAndFlush(clientTable);
        clientTableSearchRepository.save(clientTable);

        // Search the clientTable
        restClientTableMockMvc.perform(get("/api/_search/client-tables?query=id:" + clientTable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientTable.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].chairs").value(hasItem(DEFAULT_CHAIRS)))
            .andExpect(jsonPath("$.[*].modifiable").value(hasItem(DEFAULT_MODIFIABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].deleteDate").value(hasItem(sameInstant(DEFAULT_DELETE_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientTable.class);
        ClientTable clientTable1 = new ClientTable();
        clientTable1.setId(1L);
        ClientTable clientTable2 = new ClientTable();
        clientTable2.setId(clientTable1.getId());
        assertThat(clientTable1).isEqualTo(clientTable2);
        clientTable2.setId(2L);
        assertThat(clientTable1).isNotEqualTo(clientTable2);
        clientTable1.setId(null);
        assertThat(clientTable1).isNotEqualTo(clientTable2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientTableDTO.class);
        ClientTableDTO clientTableDTO1 = new ClientTableDTO();
        clientTableDTO1.setId(1L);
        ClientTableDTO clientTableDTO2 = new ClientTableDTO();
        assertThat(clientTableDTO1).isNotEqualTo(clientTableDTO2);
        clientTableDTO2.setId(clientTableDTO1.getId());
        assertThat(clientTableDTO1).isEqualTo(clientTableDTO2);
        clientTableDTO2.setId(2L);
        assertThat(clientTableDTO1).isNotEqualTo(clientTableDTO2);
        clientTableDTO1.setId(null);
        assertThat(clientTableDTO1).isNotEqualTo(clientTableDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientTableMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientTableMapper.fromId(null)).isNull();
    }
}
