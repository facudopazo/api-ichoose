package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.MealClassification;
import com.ichoose.api.repository.MealClassificationRepository;
import com.ichoose.api.service.MealClassificationService;
import com.ichoose.api.repository.search.MealClassificationSearchRepository;
import com.ichoose.api.service.dto.MealClassificationDTO;
import com.ichoose.api.service.mapper.MealClassificationMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MealClassificationResource REST controller.
 *
 * @see MealClassificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class MealClassificationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MealClassificationRepository mealClassificationRepository;

    @Autowired
    private MealClassificationMapper mealClassificationMapper;

    @Autowired
    private MealClassificationService mealClassificationService;

    @Autowired
    private MealClassificationSearchRepository mealClassificationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMealClassificationMockMvc;

    private MealClassification mealClassification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MealClassificationResource mealClassificationResource = new MealClassificationResource(mealClassificationService);
        this.restMealClassificationMockMvc = MockMvcBuilders.standaloneSetup(mealClassificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MealClassification createEntity(EntityManager em) {
        MealClassification mealClassification = new MealClassification()
            .name(DEFAULT_NAME);
        return mealClassification;
    }

    @Before
    public void initTest() {
        mealClassificationSearchRepository.deleteAll();
        mealClassification = createEntity(em);
    }

    @Test
    @Transactional
    public void createMealClassification() throws Exception {
        int databaseSizeBeforeCreate = mealClassificationRepository.findAll().size();

        // Create the MealClassification
        MealClassificationDTO mealClassificationDTO = mealClassificationMapper.toDto(mealClassification);
        restMealClassificationMockMvc.perform(post("/api/meal-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mealClassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MealClassification in the database
        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeCreate + 1);
        MealClassification testMealClassification = mealClassificationList.get(mealClassificationList.size() - 1);
        assertThat(testMealClassification.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the MealClassification in Elasticsearch
        MealClassification mealClassificationEs = mealClassificationSearchRepository.findOne(testMealClassification.getId());
        assertThat(mealClassificationEs).isEqualToIgnoringGivenFields(testMealClassification);
    }

    @Test
    @Transactional
    public void createMealClassificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mealClassificationRepository.findAll().size();

        // Create the MealClassification with an existing ID
        mealClassification.setId(1L);
        MealClassificationDTO mealClassificationDTO = mealClassificationMapper.toDto(mealClassification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMealClassificationMockMvc.perform(post("/api/meal-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mealClassificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MealClassification in the database
        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = mealClassificationRepository.findAll().size();
        // set the field null
        mealClassification.setName(null);

        // Create the MealClassification, which fails.
        MealClassificationDTO mealClassificationDTO = mealClassificationMapper.toDto(mealClassification);

        restMealClassificationMockMvc.perform(post("/api/meal-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mealClassificationDTO)))
            .andExpect(status().isBadRequest());

        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMealClassifications() throws Exception {
        // Initialize the database
        mealClassificationRepository.saveAndFlush(mealClassification);

        // Get all the mealClassificationList
        restMealClassificationMockMvc.perform(get("/api/meal-classifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mealClassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMealClassification() throws Exception {
        // Initialize the database
        mealClassificationRepository.saveAndFlush(mealClassification);

        // Get the mealClassification
        restMealClassificationMockMvc.perform(get("/api/meal-classifications/{id}", mealClassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mealClassification.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMealClassification() throws Exception {
        // Get the mealClassification
        restMealClassificationMockMvc.perform(get("/api/meal-classifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMealClassification() throws Exception {
        // Initialize the database
        mealClassificationRepository.saveAndFlush(mealClassification);
        mealClassificationSearchRepository.save(mealClassification);
        int databaseSizeBeforeUpdate = mealClassificationRepository.findAll().size();

        // Update the mealClassification
        MealClassification updatedMealClassification = mealClassificationRepository.findOne(mealClassification.getId());
        // Disconnect from session so that the updates on updatedMealClassification are not directly saved in db
        em.detach(updatedMealClassification);
        updatedMealClassification
            .name(UPDATED_NAME);
        MealClassificationDTO mealClassificationDTO = mealClassificationMapper.toDto(updatedMealClassification);

        restMealClassificationMockMvc.perform(put("/api/meal-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mealClassificationDTO)))
            .andExpect(status().isOk());

        // Validate the MealClassification in the database
        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeUpdate);
        MealClassification testMealClassification = mealClassificationList.get(mealClassificationList.size() - 1);
        assertThat(testMealClassification.getName()).isEqualTo(UPDATED_NAME);

        // Validate the MealClassification in Elasticsearch
        MealClassification mealClassificationEs = mealClassificationSearchRepository.findOne(testMealClassification.getId());
        assertThat(mealClassificationEs).isEqualToIgnoringGivenFields(testMealClassification);
    }

    @Test
    @Transactional
    public void updateNonExistingMealClassification() throws Exception {
        int databaseSizeBeforeUpdate = mealClassificationRepository.findAll().size();

        // Create the MealClassification
        MealClassificationDTO mealClassificationDTO = mealClassificationMapper.toDto(mealClassification);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMealClassificationMockMvc.perform(put("/api/meal-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mealClassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MealClassification in the database
        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMealClassification() throws Exception {
        // Initialize the database
        mealClassificationRepository.saveAndFlush(mealClassification);
        mealClassificationSearchRepository.save(mealClassification);
        int databaseSizeBeforeDelete = mealClassificationRepository.findAll().size();

        // Get the mealClassification
        restMealClassificationMockMvc.perform(delete("/api/meal-classifications/{id}", mealClassification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean mealClassificationExistsInEs = mealClassificationSearchRepository.exists(mealClassification.getId());
        assertThat(mealClassificationExistsInEs).isFalse();

        // Validate the database is empty
        List<MealClassification> mealClassificationList = mealClassificationRepository.findAll();
        assertThat(mealClassificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMealClassification() throws Exception {
        // Initialize the database
        mealClassificationRepository.saveAndFlush(mealClassification);
        mealClassificationSearchRepository.save(mealClassification);

        // Search the mealClassification
        restMealClassificationMockMvc.perform(get("/api/_search/meal-classifications?query=id:" + mealClassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mealClassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MealClassification.class);
        MealClassification mealClassification1 = new MealClassification();
        mealClassification1.setId(1L);
        MealClassification mealClassification2 = new MealClassification();
        mealClassification2.setId(mealClassification1.getId());
        assertThat(mealClassification1).isEqualTo(mealClassification2);
        mealClassification2.setId(2L);
        assertThat(mealClassification1).isNotEqualTo(mealClassification2);
        mealClassification1.setId(null);
        assertThat(mealClassification1).isNotEqualTo(mealClassification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MealClassificationDTO.class);
        MealClassificationDTO mealClassificationDTO1 = new MealClassificationDTO();
        mealClassificationDTO1.setId(1L);
        MealClassificationDTO mealClassificationDTO2 = new MealClassificationDTO();
        assertThat(mealClassificationDTO1).isNotEqualTo(mealClassificationDTO2);
        mealClassificationDTO2.setId(mealClassificationDTO1.getId());
        assertThat(mealClassificationDTO1).isEqualTo(mealClassificationDTO2);
        mealClassificationDTO2.setId(2L);
        assertThat(mealClassificationDTO1).isNotEqualTo(mealClassificationDTO2);
        mealClassificationDTO1.setId(null);
        assertThat(mealClassificationDTO1).isNotEqualTo(mealClassificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mealClassificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mealClassificationMapper.fromId(null)).isNull();
    }
}
