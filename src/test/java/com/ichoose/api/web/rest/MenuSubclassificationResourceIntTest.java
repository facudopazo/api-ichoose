package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.MenuSubclassification;
import com.ichoose.api.repository.MenuSubclassificationRepository;
import com.ichoose.api.service.MenuSubclassificationService;
import com.ichoose.api.repository.search.MenuSubclassificationSearchRepository;
import com.ichoose.api.service.dto.MenuSubclassificationDTO;
import com.ichoose.api.service.mapper.MenuSubclassificationMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MenuSubclassificationResource REST controller.
 *
 * @see MenuSubclassificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class MenuSubclassificationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MenuSubclassificationRepository menuSubclassificationRepository;

    @Autowired
    private MenuSubclassificationMapper menuSubclassificationMapper;

    @Autowired
    private MenuSubclassificationService menuSubclassificationService;

    @Autowired
    private MenuSubclassificationSearchRepository menuSubclassificationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMenuSubclassificationMockMvc;

    private MenuSubclassification menuSubclassification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MenuSubclassificationResource menuSubclassificationResource = new MenuSubclassificationResource(menuSubclassificationService);
        this.restMenuSubclassificationMockMvc = MockMvcBuilders.standaloneSetup(menuSubclassificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MenuSubclassification createEntity(EntityManager em) {
        MenuSubclassification menuSubclassification = new MenuSubclassification()
            .name(DEFAULT_NAME);
        return menuSubclassification;
    }

    @Before
    public void initTest() {
        menuSubclassificationSearchRepository.deleteAll();
        menuSubclassification = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenuSubclassification() throws Exception {
        int databaseSizeBeforeCreate = menuSubclassificationRepository.findAll().size();

        // Create the MenuSubclassification
        MenuSubclassificationDTO menuSubclassificationDTO = menuSubclassificationMapper.toDto(menuSubclassification);
        restMenuSubclassificationMockMvc.perform(post("/api/menu-subclassifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuSubclassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MenuSubclassification in the database
        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeCreate + 1);
        MenuSubclassification testMenuSubclassification = menuSubclassificationList.get(menuSubclassificationList.size() - 1);
        assertThat(testMenuSubclassification.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the MenuSubclassification in Elasticsearch
        MenuSubclassification menuSubclassificationEs = menuSubclassificationSearchRepository.findOne(testMenuSubclassification.getId());
        assertThat(menuSubclassificationEs).isEqualToIgnoringGivenFields(testMenuSubclassification);
    }

    @Test
    @Transactional
    public void createMenuSubclassificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuSubclassificationRepository.findAll().size();

        // Create the MenuSubclassification with an existing ID
        menuSubclassification.setId(1L);
        MenuSubclassificationDTO menuSubclassificationDTO = menuSubclassificationMapper.toDto(menuSubclassification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuSubclassificationMockMvc.perform(post("/api/menu-subclassifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuSubclassificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MenuSubclassification in the database
        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuSubclassificationRepository.findAll().size();
        // set the field null
        menuSubclassification.setName(null);

        // Create the MenuSubclassification, which fails.
        MenuSubclassificationDTO menuSubclassificationDTO = menuSubclassificationMapper.toDto(menuSubclassification);

        restMenuSubclassificationMockMvc.perform(post("/api/menu-subclassifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuSubclassificationDTO)))
            .andExpect(status().isBadRequest());

        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMenuSubclassifications() throws Exception {
        // Initialize the database
        menuSubclassificationRepository.saveAndFlush(menuSubclassification);

        // Get all the menuSubclassificationList
        restMenuSubclassificationMockMvc.perform(get("/api/menu-subclassifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuSubclassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMenuSubclassification() throws Exception {
        // Initialize the database
        menuSubclassificationRepository.saveAndFlush(menuSubclassification);

        // Get the menuSubclassification
        restMenuSubclassificationMockMvc.perform(get("/api/menu-subclassifications/{id}", menuSubclassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(menuSubclassification.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMenuSubclassification() throws Exception {
        // Get the menuSubclassification
        restMenuSubclassificationMockMvc.perform(get("/api/menu-subclassifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenuSubclassification() throws Exception {
        // Initialize the database
        menuSubclassificationRepository.saveAndFlush(menuSubclassification);
        menuSubclassificationSearchRepository.save(menuSubclassification);
        int databaseSizeBeforeUpdate = menuSubclassificationRepository.findAll().size();

        // Update the menuSubclassification
        MenuSubclassification updatedMenuSubclassification = menuSubclassificationRepository.findOne(menuSubclassification.getId());
        // Disconnect from session so that the updates on updatedMenuSubclassification are not directly saved in db
        em.detach(updatedMenuSubclassification);
        updatedMenuSubclassification
            .name(UPDATED_NAME);
        MenuSubclassificationDTO menuSubclassificationDTO = menuSubclassificationMapper.toDto(updatedMenuSubclassification);

        restMenuSubclassificationMockMvc.perform(put("/api/menu-subclassifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuSubclassificationDTO)))
            .andExpect(status().isOk());

        // Validate the MenuSubclassification in the database
        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeUpdate);
        MenuSubclassification testMenuSubclassification = menuSubclassificationList.get(menuSubclassificationList.size() - 1);
        assertThat(testMenuSubclassification.getName()).isEqualTo(UPDATED_NAME);

        // Validate the MenuSubclassification in Elasticsearch
        MenuSubclassification menuSubclassificationEs = menuSubclassificationSearchRepository.findOne(testMenuSubclassification.getId());
        assertThat(menuSubclassificationEs).isEqualToIgnoringGivenFields(testMenuSubclassification);
    }

    @Test
    @Transactional
    public void updateNonExistingMenuSubclassification() throws Exception {
        int databaseSizeBeforeUpdate = menuSubclassificationRepository.findAll().size();

        // Create the MenuSubclassification
        MenuSubclassificationDTO menuSubclassificationDTO = menuSubclassificationMapper.toDto(menuSubclassification);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMenuSubclassificationMockMvc.perform(put("/api/menu-subclassifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuSubclassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MenuSubclassification in the database
        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMenuSubclassification() throws Exception {
        // Initialize the database
        menuSubclassificationRepository.saveAndFlush(menuSubclassification);
        menuSubclassificationSearchRepository.save(menuSubclassification);
        int databaseSizeBeforeDelete = menuSubclassificationRepository.findAll().size();

        // Get the menuSubclassification
        restMenuSubclassificationMockMvc.perform(delete("/api/menu-subclassifications/{id}", menuSubclassification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean menuSubclassificationExistsInEs = menuSubclassificationSearchRepository.exists(menuSubclassification.getId());
        assertThat(menuSubclassificationExistsInEs).isFalse();

        // Validate the database is empty
        List<MenuSubclassification> menuSubclassificationList = menuSubclassificationRepository.findAll();
        assertThat(menuSubclassificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMenuSubclassification() throws Exception {
        // Initialize the database
        menuSubclassificationRepository.saveAndFlush(menuSubclassification);
        menuSubclassificationSearchRepository.save(menuSubclassification);

        // Search the menuSubclassification
        restMenuSubclassificationMockMvc.perform(get("/api/_search/menu-subclassifications?query=id:" + menuSubclassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuSubclassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuSubclassification.class);
        MenuSubclassification menuSubclassification1 = new MenuSubclassification();
        menuSubclassification1.setId(1L);
        MenuSubclassification menuSubclassification2 = new MenuSubclassification();
        menuSubclassification2.setId(menuSubclassification1.getId());
        assertThat(menuSubclassification1).isEqualTo(menuSubclassification2);
        menuSubclassification2.setId(2L);
        assertThat(menuSubclassification1).isNotEqualTo(menuSubclassification2);
        menuSubclassification1.setId(null);
        assertThat(menuSubclassification1).isNotEqualTo(menuSubclassification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuSubclassificationDTO.class);
        MenuSubclassificationDTO menuSubclassificationDTO1 = new MenuSubclassificationDTO();
        menuSubclassificationDTO1.setId(1L);
        MenuSubclassificationDTO menuSubclassificationDTO2 = new MenuSubclassificationDTO();
        assertThat(menuSubclassificationDTO1).isNotEqualTo(menuSubclassificationDTO2);
        menuSubclassificationDTO2.setId(menuSubclassificationDTO1.getId());
        assertThat(menuSubclassificationDTO1).isEqualTo(menuSubclassificationDTO2);
        menuSubclassificationDTO2.setId(2L);
        assertThat(menuSubclassificationDTO1).isNotEqualTo(menuSubclassificationDTO2);
        menuSubclassificationDTO1.setId(null);
        assertThat(menuSubclassificationDTO1).isNotEqualTo(menuSubclassificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(menuSubclassificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(menuSubclassificationMapper.fromId(null)).isNull();
    }
}
