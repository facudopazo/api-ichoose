package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.SectorLocation;
import com.ichoose.api.repository.SectorLocationRepository;
import com.ichoose.api.service.SectorLocationService;
import com.ichoose.api.repository.search.SectorLocationSearchRepository;
import com.ichoose.api.service.dto.SectorLocationDTO;
import com.ichoose.api.service.mapper.SectorLocationMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SectorLocationResource REST controller.
 *
 * @see SectorLocationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class SectorLocationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private SectorLocationRepository sectorLocationRepository;

    @Autowired
    private SectorLocationMapper sectorLocationMapper;

    @Autowired
    private SectorLocationService sectorLocationService;

    @Autowired
    private SectorLocationSearchRepository sectorLocationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSectorLocationMockMvc;

    private SectorLocation sectorLocation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SectorLocationResource sectorLocationResource = new SectorLocationResource(sectorLocationService);
        this.restSectorLocationMockMvc = MockMvcBuilders.standaloneSetup(sectorLocationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SectorLocation createEntity(EntityManager em) {
        SectorLocation sectorLocation = new SectorLocation()
            .name(DEFAULT_NAME);
        return sectorLocation;
    }

    @Before
    public void initTest() {
        sectorLocationSearchRepository.deleteAll();
        sectorLocation = createEntity(em);
    }

    @Test
    @Transactional
    public void createSectorLocation() throws Exception {
        int databaseSizeBeforeCreate = sectorLocationRepository.findAll().size();

        // Create the SectorLocation
        SectorLocationDTO sectorLocationDTO = sectorLocationMapper.toDto(sectorLocation);
        restSectorLocationMockMvc.perform(post("/api/sector-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sectorLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the SectorLocation in the database
        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeCreate + 1);
        SectorLocation testSectorLocation = sectorLocationList.get(sectorLocationList.size() - 1);
        assertThat(testSectorLocation.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the SectorLocation in Elasticsearch
        SectorLocation sectorLocationEs = sectorLocationSearchRepository.findOne(testSectorLocation.getId());
        assertThat(sectorLocationEs).isEqualToIgnoringGivenFields(testSectorLocation);
    }

    @Test
    @Transactional
    public void createSectorLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sectorLocationRepository.findAll().size();

        // Create the SectorLocation with an existing ID
        sectorLocation.setId(1L);
        SectorLocationDTO sectorLocationDTO = sectorLocationMapper.toDto(sectorLocation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSectorLocationMockMvc.perform(post("/api/sector-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sectorLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SectorLocation in the database
        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sectorLocationRepository.findAll().size();
        // set the field null
        sectorLocation.setName(null);

        // Create the SectorLocation, which fails.
        SectorLocationDTO sectorLocationDTO = sectorLocationMapper.toDto(sectorLocation);

        restSectorLocationMockMvc.perform(post("/api/sector-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sectorLocationDTO)))
            .andExpect(status().isBadRequest());

        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSectorLocations() throws Exception {
        // Initialize the database
        sectorLocationRepository.saveAndFlush(sectorLocation);

        // Get all the sectorLocationList
        restSectorLocationMockMvc.perform(get("/api/sector-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sectorLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSectorLocation() throws Exception {
        // Initialize the database
        sectorLocationRepository.saveAndFlush(sectorLocation);

        // Get the sectorLocation
        restSectorLocationMockMvc.perform(get("/api/sector-locations/{id}", sectorLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sectorLocation.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSectorLocation() throws Exception {
        // Get the sectorLocation
        restSectorLocationMockMvc.perform(get("/api/sector-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSectorLocation() throws Exception {
        // Initialize the database
        sectorLocationRepository.saveAndFlush(sectorLocation);
        sectorLocationSearchRepository.save(sectorLocation);
        int databaseSizeBeforeUpdate = sectorLocationRepository.findAll().size();

        // Update the sectorLocation
        SectorLocation updatedSectorLocation = sectorLocationRepository.findOne(sectorLocation.getId());
        // Disconnect from session so that the updates on updatedSectorLocation are not directly saved in db
        em.detach(updatedSectorLocation);
        updatedSectorLocation
            .name(UPDATED_NAME);
        SectorLocationDTO sectorLocationDTO = sectorLocationMapper.toDto(updatedSectorLocation);

        restSectorLocationMockMvc.perform(put("/api/sector-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sectorLocationDTO)))
            .andExpect(status().isOk());

        // Validate the SectorLocation in the database
        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeUpdate);
        SectorLocation testSectorLocation = sectorLocationList.get(sectorLocationList.size() - 1);
        assertThat(testSectorLocation.getName()).isEqualTo(UPDATED_NAME);

        // Validate the SectorLocation in Elasticsearch
        SectorLocation sectorLocationEs = sectorLocationSearchRepository.findOne(testSectorLocation.getId());
        assertThat(sectorLocationEs).isEqualToIgnoringGivenFields(testSectorLocation);
    }

    @Test
    @Transactional
    public void updateNonExistingSectorLocation() throws Exception {
        int databaseSizeBeforeUpdate = sectorLocationRepository.findAll().size();

        // Create the SectorLocation
        SectorLocationDTO sectorLocationDTO = sectorLocationMapper.toDto(sectorLocation);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSectorLocationMockMvc.perform(put("/api/sector-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sectorLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the SectorLocation in the database
        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSectorLocation() throws Exception {
        // Initialize the database
        sectorLocationRepository.saveAndFlush(sectorLocation);
        sectorLocationSearchRepository.save(sectorLocation);
        int databaseSizeBeforeDelete = sectorLocationRepository.findAll().size();

        // Get the sectorLocation
        restSectorLocationMockMvc.perform(delete("/api/sector-locations/{id}", sectorLocation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean sectorLocationExistsInEs = sectorLocationSearchRepository.exists(sectorLocation.getId());
        assertThat(sectorLocationExistsInEs).isFalse();

        // Validate the database is empty
        List<SectorLocation> sectorLocationList = sectorLocationRepository.findAll();
        assertThat(sectorLocationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSectorLocation() throws Exception {
        // Initialize the database
        sectorLocationRepository.saveAndFlush(sectorLocation);
        sectorLocationSearchRepository.save(sectorLocation);

        // Search the sectorLocation
        restSectorLocationMockMvc.perform(get("/api/_search/sector-locations?query=id:" + sectorLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sectorLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SectorLocation.class);
        SectorLocation sectorLocation1 = new SectorLocation();
        sectorLocation1.setId(1L);
        SectorLocation sectorLocation2 = new SectorLocation();
        sectorLocation2.setId(sectorLocation1.getId());
        assertThat(sectorLocation1).isEqualTo(sectorLocation2);
        sectorLocation2.setId(2L);
        assertThat(sectorLocation1).isNotEqualTo(sectorLocation2);
        sectorLocation1.setId(null);
        assertThat(sectorLocation1).isNotEqualTo(sectorLocation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SectorLocationDTO.class);
        SectorLocationDTO sectorLocationDTO1 = new SectorLocationDTO();
        sectorLocationDTO1.setId(1L);
        SectorLocationDTO sectorLocationDTO2 = new SectorLocationDTO();
        assertThat(sectorLocationDTO1).isNotEqualTo(sectorLocationDTO2);
        sectorLocationDTO2.setId(sectorLocationDTO1.getId());
        assertThat(sectorLocationDTO1).isEqualTo(sectorLocationDTO2);
        sectorLocationDTO2.setId(2L);
        assertThat(sectorLocationDTO1).isNotEqualTo(sectorLocationDTO2);
        sectorLocationDTO1.setId(null);
        assertThat(sectorLocationDTO1).isNotEqualTo(sectorLocationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sectorLocationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sectorLocationMapper.fromId(null)).isNull();
    }
}
