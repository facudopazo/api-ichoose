package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.ClientTableStatus;
import com.ichoose.api.repository.ClientTableStatusRepository;
import com.ichoose.api.service.ClientTableStatusService;
import com.ichoose.api.repository.search.ClientTableStatusSearchRepository;
import com.ichoose.api.service.dto.ClientTableStatusDTO;
import com.ichoose.api.service.mapper.ClientTableStatusMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientTableStatusResource REST controller.
 *
 * @see ClientTableStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class ClientTableStatusResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ClientTableStatusRepository clientTableStatusRepository;

    @Autowired
    private ClientTableStatusMapper clientTableStatusMapper;

    @Autowired
    private ClientTableStatusService clientTableStatusService;

    @Autowired
    private ClientTableStatusSearchRepository clientTableStatusSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientTableStatusMockMvc;

    private ClientTableStatus clientTableStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientTableStatusResource clientTableStatusResource = new ClientTableStatusResource(clientTableStatusService);
        this.restClientTableStatusMockMvc = MockMvcBuilders.standaloneSetup(clientTableStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientTableStatus createEntity(EntityManager em) {
        ClientTableStatus clientTableStatus = new ClientTableStatus()
            .name(DEFAULT_NAME);
        return clientTableStatus;
    }

    @Before
    public void initTest() {
        clientTableStatusSearchRepository.deleteAll();
        clientTableStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientTableStatus() throws Exception {
        int databaseSizeBeforeCreate = clientTableStatusRepository.findAll().size();

        // Create the ClientTableStatus
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusMapper.toDto(clientTableStatus);
        restClientTableStatusMockMvc.perform(post("/api/client-table-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientTableStatus in the database
        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ClientTableStatus testClientTableStatus = clientTableStatusList.get(clientTableStatusList.size() - 1);
        assertThat(testClientTableStatus.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the ClientTableStatus in Elasticsearch
        ClientTableStatus clientTableStatusEs = clientTableStatusSearchRepository.findOne(testClientTableStatus.getId());
        assertThat(clientTableStatusEs).isEqualToIgnoringGivenFields(testClientTableStatus);
    }

    @Test
    @Transactional
    public void createClientTableStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientTableStatusRepository.findAll().size();

        // Create the ClientTableStatus with an existing ID
        clientTableStatus.setId(1L);
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusMapper.toDto(clientTableStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientTableStatusMockMvc.perform(post("/api/client-table-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientTableStatus in the database
        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientTableStatusRepository.findAll().size();
        // set the field null
        clientTableStatus.setName(null);

        // Create the ClientTableStatus, which fails.
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusMapper.toDto(clientTableStatus);

        restClientTableStatusMockMvc.perform(post("/api/client-table-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableStatusDTO)))
            .andExpect(status().isBadRequest());

        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientTableStatuses() throws Exception {
        // Initialize the database
        clientTableStatusRepository.saveAndFlush(clientTableStatus);

        // Get all the clientTableStatusList
        restClientTableStatusMockMvc.perform(get("/api/client-table-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientTableStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getClientTableStatus() throws Exception {
        // Initialize the database
        clientTableStatusRepository.saveAndFlush(clientTableStatus);

        // Get the clientTableStatus
        restClientTableStatusMockMvc.perform(get("/api/client-table-statuses/{id}", clientTableStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientTableStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClientTableStatus() throws Exception {
        // Get the clientTableStatus
        restClientTableStatusMockMvc.perform(get("/api/client-table-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientTableStatus() throws Exception {
        // Initialize the database
        clientTableStatusRepository.saveAndFlush(clientTableStatus);
        clientTableStatusSearchRepository.save(clientTableStatus);
        int databaseSizeBeforeUpdate = clientTableStatusRepository.findAll().size();

        // Update the clientTableStatus
        ClientTableStatus updatedClientTableStatus = clientTableStatusRepository.findOne(clientTableStatus.getId());
        // Disconnect from session so that the updates on updatedClientTableStatus are not directly saved in db
        em.detach(updatedClientTableStatus);
        updatedClientTableStatus
            .name(UPDATED_NAME);
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusMapper.toDto(updatedClientTableStatus);

        restClientTableStatusMockMvc.perform(put("/api/client-table-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableStatusDTO)))
            .andExpect(status().isOk());

        // Validate the ClientTableStatus in the database
        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeUpdate);
        ClientTableStatus testClientTableStatus = clientTableStatusList.get(clientTableStatusList.size() - 1);
        assertThat(testClientTableStatus.getName()).isEqualTo(UPDATED_NAME);

        // Validate the ClientTableStatus in Elasticsearch
        ClientTableStatus clientTableStatusEs = clientTableStatusSearchRepository.findOne(testClientTableStatus.getId());
        assertThat(clientTableStatusEs).isEqualToIgnoringGivenFields(testClientTableStatus);
    }

    @Test
    @Transactional
    public void updateNonExistingClientTableStatus() throws Exception {
        int databaseSizeBeforeUpdate = clientTableStatusRepository.findAll().size();

        // Create the ClientTableStatus
        ClientTableStatusDTO clientTableStatusDTO = clientTableStatusMapper.toDto(clientTableStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientTableStatusMockMvc.perform(put("/api/client-table-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTableStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientTableStatus in the database
        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientTableStatus() throws Exception {
        // Initialize the database
        clientTableStatusRepository.saveAndFlush(clientTableStatus);
        clientTableStatusSearchRepository.save(clientTableStatus);
        int databaseSizeBeforeDelete = clientTableStatusRepository.findAll().size();

        // Get the clientTableStatus
        restClientTableStatusMockMvc.perform(delete("/api/client-table-statuses/{id}", clientTableStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clientTableStatusExistsInEs = clientTableStatusSearchRepository.exists(clientTableStatus.getId());
        assertThat(clientTableStatusExistsInEs).isFalse();

        // Validate the database is empty
        List<ClientTableStatus> clientTableStatusList = clientTableStatusRepository.findAll();
        assertThat(clientTableStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClientTableStatus() throws Exception {
        // Initialize the database
        clientTableStatusRepository.saveAndFlush(clientTableStatus);
        clientTableStatusSearchRepository.save(clientTableStatus);

        // Search the clientTableStatus
        restClientTableStatusMockMvc.perform(get("/api/_search/client-table-statuses?query=id:" + clientTableStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientTableStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientTableStatus.class);
        ClientTableStatus clientTableStatus1 = new ClientTableStatus();
        clientTableStatus1.setId(1L);
        ClientTableStatus clientTableStatus2 = new ClientTableStatus();
        clientTableStatus2.setId(clientTableStatus1.getId());
        assertThat(clientTableStatus1).isEqualTo(clientTableStatus2);
        clientTableStatus2.setId(2L);
        assertThat(clientTableStatus1).isNotEqualTo(clientTableStatus2);
        clientTableStatus1.setId(null);
        assertThat(clientTableStatus1).isNotEqualTo(clientTableStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientTableStatusDTO.class);
        ClientTableStatusDTO clientTableStatusDTO1 = new ClientTableStatusDTO();
        clientTableStatusDTO1.setId(1L);
        ClientTableStatusDTO clientTableStatusDTO2 = new ClientTableStatusDTO();
        assertThat(clientTableStatusDTO1).isNotEqualTo(clientTableStatusDTO2);
        clientTableStatusDTO2.setId(clientTableStatusDTO1.getId());
        assertThat(clientTableStatusDTO1).isEqualTo(clientTableStatusDTO2);
        clientTableStatusDTO2.setId(2L);
        assertThat(clientTableStatusDTO1).isNotEqualTo(clientTableStatusDTO2);
        clientTableStatusDTO1.setId(null);
        assertThat(clientTableStatusDTO1).isNotEqualTo(clientTableStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientTableStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientTableStatusMapper.fromId(null)).isNull();
    }
}
