package com.ichoose.api.web.rest;

import com.ichoose.api.IchooseApiApp;

import com.ichoose.api.domain.MenuClassification;
import com.ichoose.api.repository.MenuClassificationRepository;
import com.ichoose.api.service.MenuClassificationService;
import com.ichoose.api.repository.search.MenuClassificationSearchRepository;
import com.ichoose.api.service.dto.MenuClassificationDTO;
import com.ichoose.api.service.mapper.MenuClassificationMapper;
import com.ichoose.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ichoose.api.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MenuClassificationResource REST controller.
 *
 * @see MenuClassificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class MenuClassificationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MenuClassificationRepository menuClassificationRepository;

    @Autowired
    private MenuClassificationMapper menuClassificationMapper;

    @Autowired
    private MenuClassificationService menuClassificationService;

    @Autowired
    private MenuClassificationSearchRepository menuClassificationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMenuClassificationMockMvc;

    private MenuClassification menuClassification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MenuClassificationResource menuClassificationResource = new MenuClassificationResource(menuClassificationService);
        this.restMenuClassificationMockMvc = MockMvcBuilders.standaloneSetup(menuClassificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MenuClassification createEntity(EntityManager em) {
        MenuClassification menuClassification = new MenuClassification()
            .name(DEFAULT_NAME);
        return menuClassification;
    }

    @Before
    public void initTest() {
        menuClassificationSearchRepository.deleteAll();
        menuClassification = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenuClassification() throws Exception {
        int databaseSizeBeforeCreate = menuClassificationRepository.findAll().size();

        // Create the MenuClassification
        MenuClassificationDTO menuClassificationDTO = menuClassificationMapper.toDto(menuClassification);
        restMenuClassificationMockMvc.perform(post("/api/menu-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuClassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MenuClassification in the database
        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeCreate + 1);
        MenuClassification testMenuClassification = menuClassificationList.get(menuClassificationList.size() - 1);
        assertThat(testMenuClassification.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the MenuClassification in Elasticsearch
        MenuClassification menuClassificationEs = menuClassificationSearchRepository.findOne(testMenuClassification.getId());
        assertThat(menuClassificationEs).isEqualToIgnoringGivenFields(testMenuClassification);
    }

    @Test
    @Transactional
    public void createMenuClassificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuClassificationRepository.findAll().size();

        // Create the MenuClassification with an existing ID
        menuClassification.setId(1L);
        MenuClassificationDTO menuClassificationDTO = menuClassificationMapper.toDto(menuClassification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuClassificationMockMvc.perform(post("/api/menu-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuClassificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MenuClassification in the database
        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuClassificationRepository.findAll().size();
        // set the field null
        menuClassification.setName(null);

        // Create the MenuClassification, which fails.
        MenuClassificationDTO menuClassificationDTO = menuClassificationMapper.toDto(menuClassification);

        restMenuClassificationMockMvc.perform(post("/api/menu-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuClassificationDTO)))
            .andExpect(status().isBadRequest());

        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMenuClassifications() throws Exception {
        // Initialize the database
        menuClassificationRepository.saveAndFlush(menuClassification);

        // Get all the menuClassificationList
        restMenuClassificationMockMvc.perform(get("/api/menu-classifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuClassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMenuClassification() throws Exception {
        // Initialize the database
        menuClassificationRepository.saveAndFlush(menuClassification);

        // Get the menuClassification
        restMenuClassificationMockMvc.perform(get("/api/menu-classifications/{id}", menuClassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(menuClassification.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMenuClassification() throws Exception {
        // Get the menuClassification
        restMenuClassificationMockMvc.perform(get("/api/menu-classifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenuClassification() throws Exception {
        // Initialize the database
        menuClassificationRepository.saveAndFlush(menuClassification);
        menuClassificationSearchRepository.save(menuClassification);
        int databaseSizeBeforeUpdate = menuClassificationRepository.findAll().size();

        // Update the menuClassification
        MenuClassification updatedMenuClassification = menuClassificationRepository.findOne(menuClassification.getId());
        // Disconnect from session so that the updates on updatedMenuClassification are not directly saved in db
        em.detach(updatedMenuClassification);
        updatedMenuClassification
            .name(UPDATED_NAME);
        MenuClassificationDTO menuClassificationDTO = menuClassificationMapper.toDto(updatedMenuClassification);

        restMenuClassificationMockMvc.perform(put("/api/menu-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuClassificationDTO)))
            .andExpect(status().isOk());

        // Validate the MenuClassification in the database
        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeUpdate);
        MenuClassification testMenuClassification = menuClassificationList.get(menuClassificationList.size() - 1);
        assertThat(testMenuClassification.getName()).isEqualTo(UPDATED_NAME);

        // Validate the MenuClassification in Elasticsearch
        MenuClassification menuClassificationEs = menuClassificationSearchRepository.findOne(testMenuClassification.getId());
        assertThat(menuClassificationEs).isEqualToIgnoringGivenFields(testMenuClassification);
    }

    @Test
    @Transactional
    public void updateNonExistingMenuClassification() throws Exception {
        int databaseSizeBeforeUpdate = menuClassificationRepository.findAll().size();

        // Create the MenuClassification
        MenuClassificationDTO menuClassificationDTO = menuClassificationMapper.toDto(menuClassification);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMenuClassificationMockMvc.perform(put("/api/menu-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuClassificationDTO)))
            .andExpect(status().isCreated());

        // Validate the MenuClassification in the database
        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMenuClassification() throws Exception {
        // Initialize the database
        menuClassificationRepository.saveAndFlush(menuClassification);
        menuClassificationSearchRepository.save(menuClassification);
        int databaseSizeBeforeDelete = menuClassificationRepository.findAll().size();

        // Get the menuClassification
        restMenuClassificationMockMvc.perform(delete("/api/menu-classifications/{id}", menuClassification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean menuClassificationExistsInEs = menuClassificationSearchRepository.exists(menuClassification.getId());
        assertThat(menuClassificationExistsInEs).isFalse();

        // Validate the database is empty
        List<MenuClassification> menuClassificationList = menuClassificationRepository.findAll();
        assertThat(menuClassificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMenuClassification() throws Exception {
        // Initialize the database
        menuClassificationRepository.saveAndFlush(menuClassification);
        menuClassificationSearchRepository.save(menuClassification);

        // Search the menuClassification
        restMenuClassificationMockMvc.perform(get("/api/_search/menu-classifications?query=id:" + menuClassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuClassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuClassification.class);
        MenuClassification menuClassification1 = new MenuClassification();
        menuClassification1.setId(1L);
        MenuClassification menuClassification2 = new MenuClassification();
        menuClassification2.setId(menuClassification1.getId());
        assertThat(menuClassification1).isEqualTo(menuClassification2);
        menuClassification2.setId(2L);
        assertThat(menuClassification1).isNotEqualTo(menuClassification2);
        menuClassification1.setId(null);
        assertThat(menuClassification1).isNotEqualTo(menuClassification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuClassificationDTO.class);
        MenuClassificationDTO menuClassificationDTO1 = new MenuClassificationDTO();
        menuClassificationDTO1.setId(1L);
        MenuClassificationDTO menuClassificationDTO2 = new MenuClassificationDTO();
        assertThat(menuClassificationDTO1).isNotEqualTo(menuClassificationDTO2);
        menuClassificationDTO2.setId(menuClassificationDTO1.getId());
        assertThat(menuClassificationDTO1).isEqualTo(menuClassificationDTO2);
        menuClassificationDTO2.setId(2L);
        assertThat(menuClassificationDTO1).isNotEqualTo(menuClassificationDTO2);
        menuClassificationDTO1.setId(null);
        assertThat(menuClassificationDTO1).isNotEqualTo(menuClassificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(menuClassificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(menuClassificationMapper.fromId(null)).isNull();
    }
}
