package com.ichoose.api.service;

import static org.junit.Assert.*;

import org.junit.Ignore;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.ichoose.api.IchooseApiApp;
import com.ichoose.api.domain.Location;
import com.ichoose.api.web.rest.LocationResource;

/**
 * Test class for the LocationService Service.
 *
 * @see LocationSerice
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IchooseApiApp.class)
public class LocationServiceTest {
	
    @Autowired
	private LocationService locationService;

	@Test
	public void findExistingLocationByLatitudeAndLongitude() {
		final Double latitude = 40.714224;
		final Double longitude = -73.961452;
		final String formattedAddress = "277 Bedford Ave, Brooklyn, NY 11211, USA";
		final String route = "Bedford Avenue";
		final String postalCode = "11211";
		final String streetNumber = "277";
		Location location = locationService.findByLatitudeAndLongitude(latitude, longitude);
		assertThat(location.getLatitude()).isEqualTo(latitude);
		assertThat(location.getLongitude()).isEqualTo(longitude);
		assertThat(location.getFormattedAddress()).isEqualTo(formattedAddress);
		assertThat(location.getRoute()).isEqualTo(route);
		assertThat(location.getPostalCode()).isEqualTo(postalCode);
		assertThat(location.getStreetNumber()).isEqualTo(streetNumber);
		assertThat(location.getCountry()).isNotBlank().isNotNull();
	}
	
	@Test
	public void findLocationByNullableLatitudeOrLongitude(){
		final Double latitude = 40.714224;
		final Double longitude = -73.961452;
		Location location = locationService.findByLatitudeAndLongitude(latitude, null);
		assertThat(location).isNull();
		location = locationService.findByLatitudeAndLongitude(null, longitude);
		assertThat(location).isNull();
		location = locationService.findByLatitudeAndLongitude(null, null);
		assertThat(location).isNull();
	}
	
	@Test
	public void findLocationByLatitudeOrLongitudeThatIsNotAddress(){
		final Double latitude = -31.6500583;
		final Double longitude = -60.835662;
		Location location = locationService.findByLatitudeAndLongitude(latitude, longitude);
		assertThat(location).isNull();
	}

}
